﻿using EFModelExample.MySql;
using EFModelExample.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyKnight.Utils.EntityFramework;

namespace ConsoleEF
{
    class Program
    {
        static void Main(string[] args)
        {
            //var db = new SqlServerExampleDbContext();
            var db = new MySqlExampleDbContext();
            //var result = db.MyEpicProc();
            var result = db.GetNextValue<long>("TestSeq");
            Console.ReadKey();
        }
    }
}
