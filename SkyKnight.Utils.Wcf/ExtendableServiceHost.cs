﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Wcf
{
    public class ExtendableServiceHost : ServiceHost
    {
        private IEnumerable<IServiceBehavior> _serviceBehaviors;

        public ExtendableServiceHost(IEnumerable<IServiceBehavior> serviceBehaviors, Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            _serviceBehaviors = serviceBehaviors;
        }

        protected override void OnOpening()
        {
            foreach (var serviceBehavior in _serviceBehaviors)
            {
                Description.Behaviors.Add(serviceBehavior);
            }
            base.OnOpening();
        }
    }
}
