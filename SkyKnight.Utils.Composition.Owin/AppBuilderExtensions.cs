﻿using Owin;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Owin
{
    public static class AppBuilderExtensions
    {
        public static IAppBuilder UseCompositionMiddleware(this IAppBuilder app, CompositionContext container, string boundaryName = "HttpRequest")
        {
            var scopeFactory = container.CreateScopeFactory(boundaryName);

            app.Use(async (context, next) =>
            {
                using (var scope = scopeFactory.CreateExport())
                {
                    context.Set("HttpRequestScope", scope);
                    await next();
                }
            });

            return app;
        }
    }
}
