﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Owin
{
    public static class OwinContextExtensions
    {
        /// <summary>
        /// Gets the current Autofac lifetime scope from the OWIN context.
        /// </summary>
        /// <param name="context">The OWIN context.</param>
        /// <returns>The current lifetime scope.</returns>
        [SecuritySafeCritical]
        public static CompositionContext GetCompositionScope(this IOwinContext context)
        {
            if (context == null) throw new ArgumentNullException("context");

            return context.Get<CompositionContext>("HttpRequestScope");
        }
    }
}
