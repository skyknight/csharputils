﻿
interface Person   {

	PersonId:string;
	FirstName:string;
	LastName:string;
	Orders:Order[];

}
interface Order   {

	OrderId:string;
	CreatedAt:Date;
	Description:string;
	Value:number;

}
interface GenericCar   {

	GenericCarId:string;
	Name:string;

}
interface SportCar extends GenericCar  {

	AwesomnessLevel:number;

}
interface Truck extends GenericCar  {

	AcceptedMass:number;

}
   