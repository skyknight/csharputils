﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationExample.Models
{
    public class Order
    {
        public Guid OrderId { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Description { get; set; }

        public decimal Value { get; set; }
    }
}