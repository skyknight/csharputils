﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationExample.Models
{
    public class Person
    {
        public Guid PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<Order> Orders { get; set; }
    }
}