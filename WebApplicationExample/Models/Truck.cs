﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationExample.Models
{
    public class Truck : GenericCar
    {
        public decimal AcceptedMass { get; set; }
    }
}