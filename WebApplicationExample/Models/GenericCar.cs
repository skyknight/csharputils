﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationExample.Models
{
    public abstract class GenericCar
    {
        public Guid GenericCarId { get; set; }

        public string Name { get; set; }
    }
}