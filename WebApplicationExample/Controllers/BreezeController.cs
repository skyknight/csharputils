﻿using Breeze.ContextProvider;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplicationExample.Models;

namespace WebApplicationExample.Controllers
{
    public class BreezeController : ApiController
    {
        #region breeze stuff

        [HttpGet]
        public string Metadata()
        {
            return null;
        }

        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return null;
        }
        #endregion

        public IQueryable<Person> GetPersons()
        {
            return null;
        }

        public Person GetPerson(Guid id)
        {
            return null;
        }

        public IQueryable<GenericCar> Cars()
        {
            return null;
        }
    }
}
