﻿using Microsoft.AspNet.Mvc.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyKnight.Utils.AspNet.Breeze
{
    internal class AssemblyProviderManager
    {
        private static IAssemblyProvider _provider;

        public static void Register(IAssemblyProvider provider)
        {
            _provider = provider;
        }

        public static IAssemblyProvider Instance()
        {
            return _provider;
        }
    }
}
