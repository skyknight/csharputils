﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SkyKnight.Utils.AspNet.Breeze
{
    public class BreezeODataModelProvider
    {
        public static IEdmModel BuildEdmModel(Type ApiContextType)
        {
            ODataModelBuilder builder = new ODataConventionModelBuilder(AssemblyProviderManager.Instance());
            builder.Namespace = ApiContextType.Namespace;

            var publicProperties = ApiContextType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in publicProperties)
            {
                var entityClrType = TypeHelper.GetImplementedIEnumerableType(property.PropertyType);
                if (entityClrType != null)
                {
                    EntityTypeConfiguration entity = builder.AddEntityType(entityClrType);
                    builder.AddEntitySet(property.Name, entity);
                }
            }

            return builder.GetEdmModel();
        }
    }
}
