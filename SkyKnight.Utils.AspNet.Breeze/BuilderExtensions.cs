﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Mvc.Formatters;
using Microsoft.AspNet.Mvc.Infrastructure;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyKnight.Utils.AspNet.Breeze
{
    public static class BuilderExtensions
    {
        public static void AddBreeze(this IServiceCollection services)
        {
            services.AddOData();
            services.AddMvcCore(options =>
            {
                options.OutputFormatters.RemoveType<Microsoft.AspNet.OData.Formatter.ODataOutputFormatter>();
                options.OutputFormatters.RemoveType<JsonOutputFormatter>();

                var jsonOutputFormatter = new JsonOutputFormatter();
                jsonOutputFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                jsonOutputFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Include;
                jsonOutputFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
                jsonOutputFormatter.SerializerSettings.TypeNameHandling = TypeNameHandling.Objects;
                jsonOutputFormatter.SerializerSettings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                options.OutputFormatters.Insert(0, jsonOutputFormatter);
            });
        }

        public static void UseBreeze(this IApplicationBuilder app)
        {
            var defaultAssemblyProvider = app.ApplicationServices.GetRequiredService<IAssemblyProvider>();
            AssemblyProviderManager.Register(defaultAssemblyProvider);
        }
    }
}
