﻿using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Query;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc.Abstractions;
using System.Reflection;

namespace SkyKnight.Utils.AspNet.Breeze
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class EnableBreezeQueryAttribute : EnableQueryAttribute
    {
        public Type ModelType { get; set; }

        private IEdmModel _model;

        public EnableBreezeQueryAttribute(Type modelType)
        {
            ModelType = modelType;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var response = context.Result as HttpStatusCodeResult;
            if (response != null && !response.IsSuccessStatusCode())
            {
                return;
            }

            if (_model == null)
                _model = BreezeODataModelProvider.BuildEdmModel(ModelType);
            var request = context.HttpContext.Request;
            context.HttpContext.Request.ODataProperties().Model = _model;

            if (request.HasQueryOptions())
            {
                var result = context.Result as ObjectResult;
                if (result == null)
                    return;

                var value = result.Value;
                if (value != null)
                {
                    var elementClrType = TypeHelper.GetImplementedIEnumerableType(value.GetType()) ?? value.GetType();
                    var queryContext = new ODataQueryContext(
                        _model,
                        elementClrType,
                        request.ODataProperties().Path);

                    var queryOptions = new ODataQueryOptions(queryContext, request);

                    long? count = null;
                    var items = ApplyQueryOptions(result.Value, queryOptions, context.ActionDescriptor) as IEnumerable<object>;
                    if (queryOptions.Count || request.Query.ContainsKey("$inlinecount"))
                    {
                        count = Count(result.Value, queryOptions, context.ActionDescriptor);
                    }
                    // We might be getting a single result, so no paging involved
                    if (items != null)
                    {
                        result.Value = count.HasValue ? new QueryResult() { Results = items, InlineCount = count } : (object)items;
                    }
                }
            }
        }

        public override long Count(object value, ODataQueryOptions options, ActionDescriptor descriptor)
        {
            var queryableType = value.GetType().GetInterfaces().FirstOrDefault(x => x.IsConstructedGenericType && x.GetGenericTypeDefinition() == typeof(IQueryable<>));
            if(queryableType != null)
            {
                return (long)_countQuerableMethod.MakeGenericMethod(queryableType.GenericTypeArguments.First()).Invoke(this, new object[] { value, options, descriptor });
            }
            return base.Count(value, options, descriptor);
        }

        private static MethodInfo _countQuerableMethod = typeof(EnableBreezeQueryAttribute).GetMethod("CountQueryable");

        public long CountQueryable<T>(IQueryable<T> query, ODataQueryOptions options, ActionDescriptor descriptor)
        {
            var settings = new ODataQuerySettings
            {
                HandleNullPropagation = HandleNullPropagationOption.True
            };
            var forCount = options.ApplyForCount(query, settings);
            return ((IQueryable<T>)forCount).LongCount();
        }
    }
}
