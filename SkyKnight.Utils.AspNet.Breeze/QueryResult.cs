﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SkyKnight.Utils.AspNet.Breeze
{
    public class QueryResult
    {
        public dynamic Results { get; set; }
        public Int64? InlineCount { get; set; }
    }
}
