﻿using SkyKnight.Utils.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MsSql
{
    public static class DbMigrationExtensions
    {
        public static void SqlFromFile(this DbMigration dbMigration, string fileName)
        {
            if (string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["MigrationScriptsPath"]))
                throw new Exception("Missing 'MigrationScriptsPath' setting in start-up project - change project or add it to config file.");

            var fileNamePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["MigrationScriptsPath"], fileName);
            if (!File.Exists(fileNamePath))
                throw new Exception(string.Format("Path {0} not exists.", fileNamePath));
            var query = File.ReadAllText(fileNamePath);
            dbMigration.CallSql(string.Format("EXEC('{0}')", query.Replace("'", "''")));
        }

        public static void CreateSequence<T>(this DbMigration dbMigration,
            string name, T? startsWith = null, T? incrementBy = null, T? minValue = null, T? maxValue = null, bool? cycle = null) where T : struct,
          IComparable,
          IComparable<T>,
          IConvertible,
          IEquatable<T>,
          IFormattable
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");

            var dbType = string.Empty;

            if (typeof(T) == typeof(int))
                dbType = "int";
            else if (typeof(T) == typeof(byte))
                dbType = "tinyint";
            else if (typeof(T) == typeof(short))
                dbType = "smallint";
            else if (typeof(T) == typeof(long))
                dbType = "bigint";
            else if (typeof(T) == typeof(decimal))
                dbType = "decimal";
            else
                throw new ArgumentOutOfRangeException("T");

            string schemaName;
            string objectName;

            if (!SqlUtils.TryParseDatabaseObject(name, out schemaName, out objectName))
                objectName = name;

            if (string.IsNullOrWhiteSpace(schemaName))
                schemaName = "dbo";

            var sql = string.Format("CREATE SEQUENCE [{0}].[{1}] AS {2}", schemaName, objectName, dbType);

            if (startsWith.HasValue)
                sql += string.Format(" START WITH {0}", startsWith);

            if (incrementBy.HasValue)
                sql += string.Format(" INCREMENT BY {0}", incrementBy);

            if (minValue.HasValue)
                sql += string.Format(" MINVALUE {0}", minValue);

            if (maxValue.HasValue)
                sql += string.Format(" MAXVALUE {0}", maxValue);

            if (cycle.HasValue)
                sql += cycle.Value ? " CYCLE" : " NO CYCLE";

            dbMigration.CallSql(sql);

            dbMigration.CallSql(string.Format(@"EXEC('CREATE PROCEDURE [{0}].[{2}]
AS 
BEGIN
    SELECT NEXT VALUE FOR [{0}].[{1}] AS NextValue;
END')", schemaName, objectName, dbMigration.GetSequenceNextValueStoredProcedureName(objectName)));
        }

        public static void DropSequence(this DbMigration dbMigration, string name)
        {
            string schemaName;
            string objectName;

            if (!SqlUtils.TryParseDatabaseObject(name, out schemaName, out objectName))
                objectName = name;

            if (string.IsNullOrWhiteSpace(schemaName))
                schemaName = "dbo";

            dbMigration.CallSql(string.Format("DROP PROCEDURE [{0}].[{1}]", schemaName, dbMigration.GetSequenceNextValueStoredProcedureName(objectName)));
            dbMigration.CallSql(string.Format("DROP SEQUENCE [{0}].[{1}]", schemaName, objectName));
        }
    }
}
