﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MsSql
{
    public static class SqlUtils
    {
        private static Regex _databaseObjectRegex = new Regex(@"(\[{0,1}(?<Schema>\w{1,})\]{0,1}\.){0,1}\[{0,1}(?<Object>\w{1,})\]{0,1}");

        public static bool TryParseDatabaseObject(string fullObjectName, out string schemaName, out string objectName)
        {
            if (string.IsNullOrWhiteSpace(fullObjectName))
                throw new ArgumentNullException("fullObjectName");

            schemaName = objectName = null;
            var match = _databaseObjectRegex.Match(fullObjectName);
            if (match == null || !match.Success)
                return false;

            schemaName = match.Groups["Schema"] != null && !string.IsNullOrWhiteSpace(match.Groups["Schema"].Value) ? match.Groups["Schema"].Value : string.Empty;
            objectName = match.Groups["Object"].Value;
            return true;
        }
    }
}
