﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MsSql
{
    public static class DbContextExtensions
    {
        public static void SetDefaultSchema(this DbContext context, DbModelBuilder modelBuilder)
        {
            if (!(context.Database.Connection is SqlConnection))
                throw new Exception("Only SQL Server is supported for this method.");

            // polaczenie Database.Connection jest w tym miejscu zamkniete
            // z nieznanych powodow jego otwarcie uniemozliwia pobranie listy migracji
            // przez co za kazdym razem jest proba postawienia bazy 'od poczatku'
            // dlatego tworzymy nowe polaczenie na podstawie znanego connectionstringa

            using (var conn = new SqlConnection(context.Database.Connection.ConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("SELECT SCHEMA_NAME()", conn);
                var schema = cmd.ExecuteScalar();
                if (schema != DBNull.Value && schema != null && !schema.ToString().Equals("dbo", StringComparison.OrdinalIgnoreCase))
                    modelBuilder.HasDefaultSchema(schema.ToString());
            }
        }
    }
}
