﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Wcf
{
    class ComposingServiceBehavior : IServiceBehavior
    {
        private CompositionContext _compositionContext;

        public ComposingServiceBehavior(CompositionContext compositionContext)
        {
            _compositionContext = compositionContext;
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            var endpointDispatchers =
                            serviceHostBase.ChannelDispatchers.OfType<ChannelDispatcher>()
                                .SelectMany(channelDispatcher => channelDispatcher.Endpoints)

#if !NET_35
.Where(endpointDispatcher => !endpointDispatcher.IsSystemEndpoint);
#else
                    .Where(endpointDispatcher => endpointDispatcher.ContractName != "IMetadataExchange" &&
                                                 endpointDispatcher.ContractNamespace != "http://schemas.microsoft.com/2006/04/mex");
#endif

            foreach (EndpointDispatcher endpointDispatcher in endpointDispatchers)
            {
                endpointDispatcher.DispatchRuntime.InstanceProvider = new CompositionInstanceProvider(serviceDescription.ServiceType, _compositionContext);
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
        }
    }
}
