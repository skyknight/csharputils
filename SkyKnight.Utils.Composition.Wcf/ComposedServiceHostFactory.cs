﻿using SkyKnight.Utils.Wcf;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Wcf
{
    public class ComposedServiceHostFactory : ServiceHostFactory
    {
        internal static CompositionContext CompositionContext { get; set; }

        protected override System.ServiceModel.ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            if (CompositionContext == null)
                throw new Exception("CompositionContext must be setted before creating servie host instance.");

            return new ExtendableServiceHost(CompositionContext.GetExports<IServiceBehavior>(), serviceType, baseAddresses);
        }
    }
}
