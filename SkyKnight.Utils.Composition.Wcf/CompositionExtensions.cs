﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Convention;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Wcf
{
    public static class CompositionExtensions
    {
        public static void ApplyToHostFactory(this CompositionContext compositionContext, string boundaryName = Boundaries.Request)
        {
            CompositionInstanceProvider.ScopeBoundary = boundaryName;
            ComposedServiceHostFactory.CompositionContext = compositionContext;
        }

        public static ConventionBuilder ApplyWcfConvenction(this ConventionBuilder conventionBuilder)
        {
            conventionBuilder.ForType<ComposingServiceBehavior>().Export<IServiceBehavior>();
            return conventionBuilder;
        }

        public static IEnumerable<Assembly> AddWcfCompositionAssemblies(this IEnumerable<Assembly> assemblies)
        {
            var list = new List<Assembly>(assemblies);
            list.Add(typeof(CompositionExtensions).Assembly);
            list.Add(typeof(IServiceBehavior).Assembly);
            return list.Distinct();
        }
    }
}
