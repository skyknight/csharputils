﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Wcf
{
    class CompositionInstanceProvider : IInstanceProvider
    {
        public static string ScopeBoundary { get; set; }

        static CompositionInstanceProvider()
        {
            ScopeBoundary = Boundaries.Request;
        }

        readonly ExportFactory<CompositionContext> _requestScopeFactory;

        private ConcurrentDictionary<object, Export<CompositionContext>> _containers
    = new ConcurrentDictionary<object, Export<CompositionContext>>();

        private Type _serviceType;

        public CompositionInstanceProvider(Type serviceType, CompositionContext compositionContext)
        {
            _requestScopeFactory = compositionContext.CreateScopeFactory(ScopeBoundary);
            _serviceType = serviceType;
        }

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message)
        {
            var container = _requestScopeFactory.CreateExport();
            var service = container.Value.GetExport(_serviceType);
            _containers.TryAdd(service, container);

            return service;
        }

        public object GetInstance(System.ServiceModel.InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance)
        {
            Export<CompositionContext> container;
            if (_containers.TryRemove(instance, out container))
                container.Dispose();

            IDisposable disposable = instance as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }
}
