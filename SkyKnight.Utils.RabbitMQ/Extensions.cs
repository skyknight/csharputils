﻿using EasyNetQ;
using EasyNetQ.Consumer;
using EasyNetQ.FluentConfiguration;
using EasyNetQ.Producer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.RabbitMQ
{
    public static class Extensions
    {
        // https://github.com/EasyNetQ/EasyNetQ/issues/494
        #region priorities
        public static void Publish<T>(this IBus bus, T message, byte priority) where T : class
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            bus.Publish(message, bus.Advanced.Conventions.TopicNamingConvention(typeof(T)), priority);
        }

        public static void Publish<T>(this IBus bus, T message, string topic, byte priority) where T : class
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var messageType = typeof(T);
            var publishExchangeDeclareStrategy = bus.Advanced.Container.Resolve<IPublishExchangeDeclareStrategy>();
            var messageDeliveryModeStrategy = bus.Advanced.Container.Resolve<IMessageDeliveryModeStrategy>();
            var exchange = publishExchangeDeclareStrategy.DeclareExchange(bus.Advanced, messageType, "topic");

            var easyNetQMessage = new Message<T>(message)
            {
                Properties =
                {
                    DeliveryMode = messageDeliveryModeStrategy.GetDeliveryMode(messageType),
                    Priority = priority
                }
            };
            bus.Advanced.Publish(exchange, topic, false, easyNetQMessage);
        }

        public static async Task PublishAsync<T>(this IBus bus, T message, byte priority) where T : class
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            await bus.PublishAsync(message, bus.Advanced.Conventions.TopicNamingConvention(typeof(T)), priority);
        }

        public static async Task PublishAsync<T>(this IBus bus, T message, string topic, byte priority) where T : class
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));

            var messageType = typeof(T);
            var publishExchangeDeclareStrategy = bus.Advanced.Container.Resolve<IPublishExchangeDeclareStrategy>();
            var messageDeliveryModeStrategy = bus.Advanced.Container.Resolve<IMessageDeliveryModeStrategy>();
            var exchange = await publishExchangeDeclareStrategy.DeclareExchangeAsync(bus.Advanced, messageType, "topic");

            var easyNetQMessage = new Message<T>(message)
            {
                Properties =
                {
                    DeliveryMode = messageDeliveryModeStrategy.GetDeliveryMode(messageType),
                    Priority = priority
                }
            };
            await bus.Advanced.PublishAsync(exchange, topic, false, easyNetQMessage);
        }

        public static ISubscriptionResult SubscribeAsync<TMessage>(this IBus bus, string subscriptionId,
            Func<TMessage, Task> onMessage, byte maxPriority) where TMessage : class
        {
            return bus.SubscribeAsync(subscriptionId, onMessage, x => { }, maxPriority);
        }

        public static ISubscriptionResult SubscribeAsync<TMessage>(this IBus bus, string subscriptionId,
            Func<TMessage, Task> onMessage, Action<ISubscriptionConfiguration> configure, byte maxPriority) where TMessage : class
        {
            var connectionConfiguration = bus.Advanced.Container.Resolve<ConnectionConfiguration>();
            var configuration = new SubscriptionConfiguration(connectionConfiguration.PrefetchCount);
            configure(configuration);

            var queueName = bus.Advanced.Conventions.QueueNamingConvention(typeof(TMessage), subscriptionId);
            var exchangeName = bus.Advanced.Conventions.ExchangeNamingConvention(typeof(TMessage));

            var queue = bus.Advanced.QueueDeclare(queueName, autoDelete: configuration.AutoDelete, expires: configuration.Expires, maxPriority: maxPriority);
            var exchange = bus.Advanced.ExchangeDeclare(exchangeName, "topic");

            var topics = configuration.Topics;
            if (!topics.Any())
                topics.Add("#");

            foreach (var topic in topics)
            {
                bus.Advanced.Bind(exchange, queue, topic);
            }

            var consumerCancellation = bus.Advanced.Consume<TMessage>(
                queue,
                (message, messageReceivedInfo) => onMessage(message.Body),
                x =>
                {
                    x.WithPriority(configuration.Priority)
                        .WithCancelOnHaFailover(configuration.CancelOnHaFailover)
                        .WithPrefetchCount(configuration.PrefetchCount);
                    if (configuration.IsExclusive)
                    {
                        x.AsExclusive();
                    }
                });
            return new SubscriptionResult(exchange, queue, consumerCancellation);
        }
        #endregion
    }
}
