﻿using SkyKnight.Utils.Composition.Configuration;
using System;
using System.Collections.Generic;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SkyKnight.Utils.Composition.Tests
{
    public class CompositionExportTests
    {
        [Fact]
        public void Export_interfaces_via_config()
        {
            var cfgConventions = new[]
            {
                new ConventionEntry()
                {
                    ExportInterfaces = true,
                    ForTypesDerivedFrom = typeof(IBase)
                }
            };

            var conventions = new ExtendedConventionBuilder();
            conventions.ApplyConventions(cfgConventions);

            var configuration = new ContainerConfiguration()
                .WithAssemblies(new[] { typeof(CompositionExportTests).Assembly }, conventions);

            var container = configuration.CreateContainer();

            var test = container.GetExport<IFoo>();
            Assert.NotNull(test);
        }
    }

    interface IBase { }

    interface IFoo : IBase { }

    interface IBar : IBase { }

    class FooImpl : IFoo { }

    class BarImpl : IBar { }
}
