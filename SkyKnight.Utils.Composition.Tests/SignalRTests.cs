﻿using SkyKnight.Utils.Composition.SignalR;
using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using Xunit;

namespace SkyKnight.Utils.Composition.Tests
{
    public class SignalRTests
    {
        [Fact]
        public void Build_Composition_And_Retrieve_ScopeManager()
        {
            var composition = BuildCompositionHost();

            Assert.NotNull(composition.GetExport<IScopeManager>());
        }

        private CompositionHost BuildCompositionHost()
        {
            var conventions = new ConventionBuilder();
            conventions.ApplySignalRConventions();

            var container = new ContainerConfiguration()
                            .WithAssemblies(new[]
            {
                Assembly.GetExecutingAssembly(),
                typeof(BaseHub).Assembly,
                typeof(Microsoft.AspNet.SignalR.Hub).Assembly
            }, conventions)
            .CreateContainer();

            return container;
        }

    }
}
