﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SkyKnight.Utils;

namespace SkyKnight.Utils.Composition.Tests
{
    public class CompositionTests
    {

        [Fact]
        public void Generic_Support_Test()
        {
            var conventions = new ExtendedConventionBuilder();

            conventions.ForType(typeof(MyContainer<>)).Export().SelectConstructor(cs => cs.First());
            conventions.ForType<MyValue>().Export();

            var configuration = new ContainerConfiguration()
    .WithAssemblies(new[] { typeof(CompositionTests).Assembly }, conventions);

            var container = configuration.CreateContainer();

            var test = container.GetExport<MyContainer<MyValue>>();
            Assert.NotNull(test);
        }

        [Fact]
        public void Taki_tam_test()
        {
            Assert.Equal(typeof(MyContainer<>).GetConstructors().First(), typeof(MyContainer<MyValue>).GetGenericTypeDefinition().GetConstructors().First());
        }

        [Fact]
        public void Exporting_Properties_With_Generic()
        {
            var conventions = new ConventionBuilder();
            conventions.ForType<Stuff>().ExportPropertiesWithContractNames(typesForContractNames: new[] { typeof(IReadOnlyDictionary<,>), typeof(IDictionary<,>) }).Shared();

                        var configuration = new ContainerConfiguration()
    .WithAssemblies(new[] { typeof(CompositionTests).Assembly }, conventions);

            var container = configuration.CreateContainer();

            var test = container.GetExport<IReadOnlyDictionary<string, string>>("ReadOnlyDic");


            Assert.NotNull(test);
        }

        [Fact]
        public void Get_Last_Export_For_Many_From_ServiceProvider()
        {
            var conventions = new ConventionBuilder();
            conventions.ForType<FirstService>().Export<IMyService>();
            conventions.ForType<SecondService>().Export<IMyService>();

            var configuration = new ContainerConfiguration()
.WithAssemblies(new[] { typeof(CompositionTests).Assembly }, conventions);

            var container = configuration.CreateContainer();
            var serviceProvider = new CompositionServiceProvider(container);
            var myService = serviceProvider.GetService(typeof(IMyService));
            Assert.IsType<SecondService>(myService);
        }

        interface IMyService
        { }

        class FirstService : IMyService
        { }

        class SecondService : IMyService
        { }

        class MyValue
        {
            public int Sth { get; set; }
        }

        class MyContainer<T>
        {
            public IEnumerable<T> Value { get; set; }

            //[ImportingConstructor]
            public MyContainer(IEnumerable<T> value)
            {
                Value = value;
            }
        }

        class Stuff
        {
            public IDictionary<string, string> Dic
            {
                get
                {
                    return new Dictionary<string, string>()
                        {
                            { "key", "value" }
                        };
                }
            }

            public IReadOnlyDictionary<string, string> ReadOnlyDic
            {
                get { return Dic.AsReadOnly(); }
            }
        }
    }
}
