﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.AspNet
{
    public class CompositionServiceScope : IServiceScope
    {
        private Export<CompositionContext> _scope;

        public CompositionServiceScope(ExportFactory<CompositionContext> scopeFactory)
        {
            _scope = scopeFactory.CreateExport();
            ServiceProvider = new CompositionServiceProvider(_scope.Value);
        }

        public IServiceProvider ServiceProvider
        {
            get; private set;
        }

        public void Dispose()
        {
            _scope.Dispose();
        }
    }
}
