﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.AspNet
{
    public class DI
    {
        private IEnumerable<Assembly> _assemblies;
        private IEnumerable<ServiceDescriptor> _descriptors;
        private Action<ConventionBuilder> _conventionAction;
        private static List<Type> _basicTypes = new List<Type>()
        {
            typeof(string),
            typeof(byte),
            typeof(short),
            typeof(int),
            typeof(long),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(DateTime)
        };

        public DI(IEnumerable<Assembly> assemblies)
        {
            if (assemblies == null)
                throw new ArgumentNullException("assemblies");

            _assemblies = assemblies;
        }

        public DI()
            : this(new[] 
            {
#if DNX451
                Assembly.GetExecutingAssembly(),
#endif
                typeof(DI).GetTypeInfo().Assembly,
                typeof(CompositionServiceProvider).GetTypeInfo().Assembly
            })
        { }

        public void Populate(IEnumerable<ServiceDescriptor> descriptors)
        {
            if (descriptors == null)
                throw new ArgumentNullException("descriptors");

            _descriptors = descriptors;
        }

        public void UseConventions(Action<ConventionBuilder> conventionAction)
        {
            _conventionAction = conventionAction;
        }

        public IServiceProvider Build()
        {
            var conventions = new ExtendedConventionBuilder();
            conventions.ForType<CompositionServiceProvider>().Export<IServiceProvider>().Shared();
            conventions.ForType<CompositionServiceScopeFactory>().Export<IServiceScopeFactory>().Shared();

            var assemblies = new List<Assembly>();
            assemblies.AddRange(_assemblies);
            var declaredTypes = new List<Tuple<Type, Type>>();

            if (_descriptors != null)
            {
                assemblies.AddRange(_descriptors.Select(d => d.ServiceType.GetTypeInfo().Assembly));
                assemblies.AddRange(_descriptors.Where(d => d.ImplementationType != null).Select(d => d.ImplementationType.GetTypeInfo().Assembly));

                foreach (var descriptor in _descriptors.Where(d => d.ImplementationFactory == null && d.ImplementationInstance == null))
                {
                    var implType = descriptor.ImplementationType.IsConstructedGenericType 
                        ? descriptor.ImplementationType.GetGenericTypeDefinition() : descriptor.ImplementationType;
                    var servType = descriptor.ImplementationType.IsConstructedGenericType && descriptor.ServiceType.IsConstructedGenericType
                        ? descriptor.ServiceType.GetGenericTypeDefinition() : descriptor.ServiceType;

                    if (declaredTypes.Any(dt => dt.Item1 == implType && dt.Item2 == servType))
                        continue;

                    var builder = conventions.ForType(implType);
                    builder = descriptor.ServiceType != null ? builder.Export(ec => ec.AsContractType(servType)) : builder.Export();
                    if (descriptor.Lifetime == ServiceLifetime.Singleton)
                        builder = builder.Shared();
                    else if (descriptor.Lifetime == ServiceLifetime.Scoped)
                        builder = builder.Shared(Boundaries.Request);
                    builder.SelectConstructor(cs => cs
                        .OrderByDescending(c => c.GetParameters().Length)
                        .Where(c => c.IsPublic)
                        .Select(c => new { Constructor = c, ComplexParameters = c.GetParameters().Count(p => !_basicTypes.Contains(p.ParameterType)) })
                        .OrderByDescending(c => c.ComplexParameters)
                        .Select(c => c.Constructor)
                        .First(), (pi, icb) =>
                    {
                        if (pi.ParameterType == typeof(IEnumerable<>))
                            icb.AsMany();
                    });
                    declaredTypes.Add(new Tuple<Type, Type>(implType, servType));
                }
            }

            if (_conventionAction != null)
                _conventionAction(conventions);

            var configuration = new ContainerConfiguration()
                .WithAssemblies(assemblies.Distinct(), conventions);

            if(_descriptors != null)
            {
                foreach (var descriptor in _descriptors.Where(d => d.ImplementationFactory != null || d.ImplementationInstance != null))
                {
                    configuration.WithFactoryDelegate(
                        descriptor.ImplementationFactory != null 
                        ? (Func<CompositionContext, object>)(c => descriptor.ImplementationFactory(new CompositionServiceProvider(c))) 
                        : c => descriptor.ImplementationInstance, 
                        descriptor.ServiceType ?? descriptor.ImplementationType, 
                        isShared: (descriptor.Lifetime == ServiceLifetime.Scoped || descriptor.Lifetime == ServiceLifetime.Singleton),
                        boundaryName: descriptor.Lifetime == ServiceLifetime.Scoped ? Boundaries.Request : null);
                }
            }

            var container = configuration.CreateContainer();

            return container.GetExport<IServiceProvider>();
        }
    }
}
