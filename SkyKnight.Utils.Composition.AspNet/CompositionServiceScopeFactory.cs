﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.AspNet
{
    public class CompositionServiceScopeFactory : IServiceScopeFactory
    {
        private ExportFactory<CompositionContext> _scopeFactory;

        public CompositionServiceScopeFactory(CompositionContext compositionContext)
        {
            _scopeFactory = compositionContext.CreateScopeFactory(Boundaries.Request);
        }

        public IServiceScope CreateScope()
        {
            return new CompositionServiceScope(_scopeFactory);
        }
    }
}
