set /p DNX_BUILD_VERSION=<buildno.txt

if defined DNX_BUILD_VERSION ( 
    set /a DNX_BUILD_VERSION = %DNX_BUILD_VERSION% + 1
) else (
    set /a DNX_BUILD_VERSION=1
)

echo %DNX_BUILD_VERSION% > buildno.txt