﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SkyKnight.Utils.Composition.AspNet.Tests
{
    public class DITests
    {
        public DITests()
        {
        }

        [Fact]
        public void ShouldChooseConstructorWithComplexType()
        {
            var di = new DI();
            di.Populate(new[]
            {
                new ServiceDescriptor(typeof(Foo), typeof(Foo), ServiceLifetime.Singleton),
                new ServiceDescriptor(typeof(Bar), typeof(Bar), ServiceLifetime.Singleton)
            });
            var cntnr = di.Build();
            var bar = cntnr.GetService<Bar>();
            Assert.NotNull(bar);
        }

        class Foo
        {

        }

        class Bar
        {
            public Bar(string str)
            {

            }

            public Bar(Foo foo)
            {

            }
        }
    }
}
