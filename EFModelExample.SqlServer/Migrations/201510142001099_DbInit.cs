namespace EFModelExample.SqlServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        OrderItemId = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        Order_OrderId = c.Guid(),
                    })
                .PrimaryKey(t => t.OrderItemId)
                .ForeignKey("dbo.Orders", t => t.Order_OrderId)
                .Index(t => t.Order_OrderId);
            
            CreateTable(
                "dbo.OrderPersons",
                c => new
                    {
                        OrderPersonId = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                        PersonId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.OrderPersonId)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                        Name = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderId);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PersonId = c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"),
                        Name = c.String(),
                        BirthDay = c.DateTime()
                    })
                .PrimaryKey(t => t.PersonId);

            Sql("CREATE VIEW dbo.PeopleView AS SELECT p.PersonId, p.Name, p.BirthDay, (SELECT COUNT(*) FROM OrderPersons op WHERE op.PersonId = p.PersonId) AS OrdersCount FROM People p");
            
            CreateStoredProcedure(
                "dbo.Person_Insert",
                p => new
                    {
                        Name = p.String(),
                        BirthDay = p.DateTime(),
                    },
                body:
                    @"DECLARE @generated_keys table([PersonId] uniqueidentifier)
                      INSERT [dbo].[People]([Name], [BirthDay])
                      OUTPUT inserted.[PersonId] INTO @generated_keys
                      VALUES (@Name, @BirthDay)
                      
                      DECLARE @PersonId uniqueidentifier
                      SELECT @PersonId = t.[PersonId]
                      FROM @generated_keys AS g JOIN [dbo].[People] AS t ON g.[PersonId] = t.[PersonId]
                      WHERE @@ROWCOUNT > 0
                      
                      SELECT t0.[PersonId], t0.[OrdersCount]
                      FROM [dbo].[PeopleView] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PersonId] = @PersonId"
            );
            
            CreateStoredProcedure(
                "dbo.Person_Update",
                p => new
                    {
                        PersonId = p.Guid(),
                        Name = p.String(),
                        BirthDay = p.DateTime(),
                    },
                body:
                    @"UPDATE [dbo].[People]
                      SET [Name] = @Name, [BirthDay] = @BirthDay
                      WHERE ([PersonId] = @PersonId)
                      
                      SELECT t0.[OrdersCount]
                      FROM [dbo].[PeopleView] AS t0
                      WHERE @@ROWCOUNT > 0 AND t0.[PersonId] = @PersonId"
            );
            
            CreateStoredProcedure(
                "dbo.Person_Delete",
                p => new
                    {
                        PersonId = p.Guid(),
                    },
                body:
                    @"DELETE [dbo].[People]
                      WHERE ([PersonId] = @PersonId)"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Person_Delete");
            DropStoredProcedure("dbo.Person_Update");
            DropStoredProcedure("dbo.Person_Insert");
            DropForeignKey("dbo.OrderPersons", "PersonId", "dbo.PeopleView");
            DropForeignKey("dbo.OrderPersons", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderItems", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderPersons", new[] { "OrderId" });
            DropIndex("dbo.OrderPersons", new[] { "PersonId" });
            DropIndex("dbo.OrderItems", new[] { "Order_OrderId" });
            DropTable("dbo.PeopleView");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderPersons");
            DropTable("dbo.OrderItems");
        }
    }
}
