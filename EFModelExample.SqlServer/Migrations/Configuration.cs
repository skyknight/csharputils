namespace EFModelExample.SqlServer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EFModelExample.SqlServer.SqlServerExampleDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EFModelExample.SqlServer.SqlServerExampleDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Persons.AddOrUpdate(
              p => p.Name,
              new Person { Name = "Andrew Peters", BirthDay = DateTime.Today.AddYears(-19) },
              new Person { Name = "Brice Lambson", BirthDay = DateTime.Today.AddYears(-16) },
              new Person { Name = "Rowan Miller", BirthDay = DateTime.Today.AddYears(-13) }
            );
            //
        }
    }
}
