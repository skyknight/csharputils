﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition
{
    public static class ConventionBuilderExtensions
    {
        public static PartConventionBuilder ExportPropertiesWithContractNames(
            this PartConventionBuilder pcBuilder, 
            Predicate<PropertyInfo> propertyFilter = null, 
            IEnumerable<Type> typesForContractNames = null,
            Func<string, string> contractNameFormatter = null)
        {
            CheckForValuesTypes(typesForContractNames);

            return pcBuilder.ExportProperties(propertyFilter ?? new Predicate<PropertyInfo>(pi => true), (pi, ecb) =>
                {
                    if (typesForContractNames == null 
                        || typesForContractNames.Any(t => t == pi.PropertyType || (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == t)))
                        ecb.AsContractName(contractNameFormatter != null ? contractNameFormatter(pi.Name) : pi.Name).AsContractType(pi.PropertyType);
                });
        }

        public static PartConventionBuilder SelectConstructorWithContractNames(
            this PartConventionBuilder pcBuilder,
            Func<IEnumerable<ConstructorInfo>, ConstructorInfo> constructorSelector = null,
            IEnumerable<Type> typesForContractNames = null,
            Func<string, string> contractNameFormatter = null)
        {
            CheckForValuesTypes(typesForContractNames);

            return pcBuilder.SelectConstructor(
                constructorSelector ?? new Func<IEnumerable<ConstructorInfo>, ConstructorInfo>(ci => ci.OrderByDescending(c => c.GetParameters().Length).First(c => c.IsPublic)),
                (pi, icb) =>
                {
                    if (typesForContractNames == null || typesForContractNames.Any(t => t == pi.ParameterType || (pi.ParameterType.IsGenericType && pi.ParameterType.GetGenericTypeDefinition() == t)))
                        icb.AsContractName(contractNameFormatter != null ? contractNameFormatter(pi.Name) : pi.Name);
                });
        }

        private static void CheckForValuesTypes(IEnumerable<Type> types)
        {
            if (types != null && types.Any(t => t.IsValueType))
                throw new NotSupportedException("Values types are not supported for now.");
        }
    }
}
