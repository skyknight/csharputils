﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition
{
    public static class CompositionExtensions
    {
        public static ExportFactory<CompositionContext> CreateScopeFactory(this CompositionContext compositionContext, string boundaryName)
        {
            if (compositionContext == null)
                throw new ArgumentNullException("compositionContext");

            var factoryContract = new CompositionContract(typeof(ExportFactory<CompositionContext>), null, new Dictionary<string, object>
            {
                { "SharingBoundaryNames", new[] { boundaryName } }
            });
            return (ExportFactory<CompositionContext>)compositionContext.GetExport(factoryContract);
        }
    }
}
