﻿using System;
using System.Collections.Generic;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public static class ContainerConfigurationExtensions
    {
        public static ContainerConfiguration ApplyFactories(this ContainerConfiguration configuration, IEnumerable<FactoryEntry> factories)
        {
            foreach (var fe in factories)
            {
                configuration.WithFactoryDelegate(fe.Factory, fe.ExportType, string.IsNullOrWhiteSpace(fe.ContractName) ? null : fe.ContractName,
                    fe.Metadata, fe.Shared, string.IsNullOrWhiteSpace(fe.Boundary) ? null : fe.Boundary);
            }
            return configuration;
        }
    }
}
