﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionConstructorContractElement : ConfigurationElement
    {
        [ConfigurationProperty("paramName", IsRequired = true)]
        public string ParamName
        {
            get { return (string)this["paramName"]; }
            set
            {
                this["paramName"] = value;
            }
        }

        [ConfigurationProperty("contractName", IsRequired = true)]
        public string ContractName
        {
            get { return (string)this["contractName"]; }
            set
            {
                this["contractName"] = value;
            }
        }
    }
}
