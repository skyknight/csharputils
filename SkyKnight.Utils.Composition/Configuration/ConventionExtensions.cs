﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public static class ConventionExtensions
    {
        public static ConventionBuilder ApplyConventions(this ConventionBuilder conventionBuilder, IEnumerable<ConventionEntry> conventionsEntries)
        {
            foreach (var ce in conventionsEntries)
            {
                PartConventionBuilder partConvention = null;

                if (ce.ForType != null)
                    partConvention = conventionBuilder.ForType(ce.ForType);
                else if (ce.ForTypesDerivedFrom != null)
                    partConvention = conventionBuilder.ForTypesDerivedFrom(ce.ForTypesDerivedFrom);

                if (partConvention == null)
                    continue;

                if((ce.ConstructorContracts != null && ce.ConstructorContracts.Count > 0) || ce.ConstructorMatching != ConventionConstructorMatching.Default)
                {
                    Func<IEnumerable<ConstructorInfo>, ConstructorInfo> cnstrSelector = null;
                    if (ce.ConstructorMatching == ConventionConstructorMatching.Empty)
                        cnstrSelector = cs => cs.First(cnstr => cnstr.GetParameters().Length == 0 && cnstr.IsPublic);
                    else if (ce.ConstructorMatching == ConventionConstructorMatching.Exact)
                        cnstrSelector = cs => cs.First(cnstr => cnstr.GetParameters().Length == ce.ConstructorContracts.Count && cnstr.IsPublic);
                    else
                        cnstrSelector = cs => cs.OrderByDescending(cnstr => cnstr.GetParameters().Length).First(cnstr => cnstr.IsPublic);

                    partConvention.SelectConstructor(cnstrSelector, (pi, ic) =>
                    {
                        string contractName;
                        if (ce.ConstructorContracts.TryGetValue(pi.Name, out contractName) && !string.IsNullOrWhiteSpace(contractName))
                            ic.AsContractName(contractName);
                    });
                }

                if (!string.IsNullOrWhiteSpace(ce.Boundary))
                    partConvention.Shared(ce.Boundary);
                else if (ce.Shared)
                    partConvention.Shared();

                if (ce.Metadata != null)
                    foreach (var meta in ce.Metadata)
                        partConvention.AddPartMetadata(meta.Key, meta.Value);

                if (ce.Export)
                    partConvention.Export(ecb =>
                    {
                        if (ce.ExportType != null)
                            ecb.AsContractType(ce.ExportType);

                        if (!string.IsNullOrWhiteSpace(ce.ContractName))
                            ecb.AsContractName(ce.ContractName);

                        if (ce.Metadata != null)
                            foreach (var meta in ce.Metadata)
                                ecb.AddMetadata(meta.Key, meta.Value);
                    });

                if(ce.ExportProperties)
                {
                    if (ce.ExportPropertiesContracts != null && ce.ExportPropertiesContracts.Any())
                    {
                        partConvention.ExportProperties(pi => ce.ExportPropertiesContracts.ContainsKey(pi.Name),
                            (pi, ecb) =>
                            {
                                var contractName = ce.ExportPropertiesContracts[pi.Name];
                                if(!string.IsNullOrWhiteSpace(contractName))
                                    ecb.AsContractName(contractName);
                            });
                    }
                    else
                        partConvention.ExportProperties(pi => true);
                }

                if (ce.ExportInterfaces)
                    partConvention.ExportInterfaces(t => true, (t, ecb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(ce.ContractName))
                            ecb.AsContractName(ce.ContractName);

                        if (ce.Metadata != null)
                            foreach (var meta in ce.Metadata)
                                ecb.AddMetadata(meta.Key, meta.Value);
                    });
            }

            return conventionBuilder;
        }
    }
}
