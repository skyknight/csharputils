﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class ConventionEntry
    {
        public Type ForTypesDerivedFrom { get; set; }

        public Type ForType { get; set; }

        public Type ExportType { get; set; }

        public bool Export { get; set; }

        public bool ExportProperties { get; set; }

        public IReadOnlyDictionary<string, string> ExportPropertiesContracts { get; set; }

        public bool ExportInterfaces { get; set; }

        public bool Shared { get; set; }

        public string Boundary { get; set; }

        public string ContractName { get; set; }

        public ConventionConstructorMatching ConstructorMatching { get; set; }

        public IReadOnlyDictionary<string, string> ConstructorContracts { get; set; }

        public IReadOnlyDictionary<string, string> Metadata { get; set; }
    }

    public enum ConventionConstructorMatching
    {
        Default,
        Empty,
        Exact,
        Complex
    }
}
