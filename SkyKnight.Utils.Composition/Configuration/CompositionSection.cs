﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionSection : ConfigurationSection
    {
        [ConfigurationProperty("conventions", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(CompositionConventionElement))]
        public CompositionConventionElementCollection Conventions
        {
            get { return (CompositionConventionElementCollection)this["conventions"]; }
            set { this["conventions"] = value; }
        }

        [ConfigurationProperty("factories", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(CompositionFactoryElement))]
        public CompositionFactoryElementCollection Factories
        {
            get { return (CompositionFactoryElementCollection)this["factories"]; }
            set { this["factories"] = value; }
        }
    }
}
