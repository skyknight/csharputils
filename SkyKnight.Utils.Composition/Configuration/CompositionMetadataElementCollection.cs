﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionMetadataElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CompositionMetadataElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CompositionMetadataElement)element).Key;
        }
    }
}
