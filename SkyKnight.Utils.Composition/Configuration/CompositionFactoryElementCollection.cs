﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionFactoryElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CompositionFactoryElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CompositionFactoryElement)element).Key;
        }
    }
}
