﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionConventionElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CompositionConventionElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CompositionConventionElement)element).Key;
        }
    }
}
