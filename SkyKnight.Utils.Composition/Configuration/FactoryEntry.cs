﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class FactoryEntry
    {
        public Type ExportType { get; set; }

        public Func<CompositionContext, object> Factory { get; set; }

        public bool Shared { get; set; }

        public string Boundary { get; set; }

        public string ContractName { get; set; }

        public IDictionary<string, object> Metadata { get; set; }
    }
}
