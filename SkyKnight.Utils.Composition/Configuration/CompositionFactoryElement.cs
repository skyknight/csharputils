﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionFactoryElement : ConfigurationElement
    {
        [ConfigurationProperty("factoryType", IsRequired = true)]
        public string FactoryType
        {
            get { return (string)this["factoryType"]; }
            set
            {
                this["factoryType"] = value;
            }
        }

        [ConfigurationProperty("factoryMethod", IsRequired = true)]
        public string FactoryMethod
        {
            get { return (string)this["factoryMethod"]; }
            set
            {
                this["factoryMethod"] = value;
            }
        }

        [ConfigurationProperty("shared", IsRequired = false)]
        public bool Shared
        {
            get { return (bool)this["shared"]; }
            set
            {
                this["shared"] = value;
            }
        }

        [ConfigurationProperty("boundary", IsRequired = false)]
        public string Boundary
        {
            get { return (string)this["boundary"]; }
            set
            {
                this["boundary"] = value;
            }
        }

        [ConfigurationProperty("contractName", IsRequired = false)]
        public string ContractName
        {
            get { return (string)this["contractName"]; }
            set
            {
                this["contractName"] = value;
            }
        }

        [ConfigurationProperty("metadata")]
        public CompositionMetadataElementCollection Metadata
        {
            get { return (CompositionMetadataElementCollection)this["metadata"]; }
            set { this["metadata"] = value; }
        }

        public string Key
        {
            get { return string.Format("{0}_{1}_{2}", FactoryType, FactoryMethod, ContractName); }
        }
    }
}
