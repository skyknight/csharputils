﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public static class ConfigurationManagerCompositionExtensions
    {
        private static readonly char[] _contractSplitters = new[] { ',', ';' };

        public static ConventionBuilder ApplyConventionFromCurrentConfig(this ConventionBuilder conventionBuilder, string sectionName = "composition")
        {
            var section = (CompositionSection)ConfigurationManager.GetSection(sectionName);

            return ApplyConvention(conventionBuilder, section.Conventions.Cast<CompositionConventionElement>());
        }

        public static ConventionBuilder ApplyConventions(this ConventionBuilder conventionBuilder, 
            System.Configuration.Configuration configuration, string sectionName = "composition")
        {
            var section = (CompositionSection)configuration.GetSection(sectionName);

            return ApplyConvention(conventionBuilder, section.Conventions.Cast<CompositionConventionElement>());
        }

        private static ConventionBuilder ApplyConvention(ConventionBuilder conventionBuilder, IEnumerable<CompositionConventionElement> configConventions)
        {
            var entries = new List<ConventionEntry>();

            foreach (var cc in configConventions)
            {
                var entry = new ConventionEntry();
                entry.Boundary = cc.Boundary;
                entry.ContractName = cc.ContractName;
                entry.Shared = cc.Shared;

                if (!string.IsNullOrWhiteSpace(cc.ForType))
                    entry.ForType = TryGetType(cc.ForType);

                if (!string.IsNullOrWhiteSpace(cc.ForTypesDerivedFrom))
                    entry.ForTypesDerivedFrom = TryGetType(cc.ForTypesDerivedFrom);

                var constructorMatching = ConventionConstructorMatching.Default;

                if (!string.IsNullOrWhiteSpace(cc.ConstructorMatching))
                    Enum.TryParse(cc.ConstructorMatching, true, out constructorMatching);

                entry.ConstructorMatching = constructorMatching;

                entry.ConstructorContracts = cc.ConstructorContracts.Split(_contractSplitters, StringSplitOptions.RemoveEmptyEntries)
                    .Select(s =>
                    {
                        var idx = s.IndexOf('=');
                        return idx >= 0 
                            ? new KeyValuePair<string, string>(s.Substring(0, idx), s.Substring(idx + 1)) 
                            : new KeyValuePair<string, string>(s, string.Empty);
                    })
                    .ToDictionary(kvp => kvp.Key, kvp =>kvp.Value)
                    .AsReadOnly();

                var export = false;
                Type exportType = null;

                if(!string.IsNullOrWhiteSpace(cc.Export) && !bool.TryParse(cc.Export, out export))
                {
                    export = true;
                    exportType = TryGetType(cc.Export);
                }

                var exportProperties = false;
                IReadOnlyDictionary<string, string> exportPropertiesContracts = null;

                if(!string.IsNullOrWhiteSpace(cc.ExportProperties) && !bool.TryParse(cc.ExportProperties, out exportProperties))
                {
                    exportProperties = true;
                    exportPropertiesContracts = cc.ExportProperties.Split(_contractSplitters, StringSplitOptions.RemoveEmptyEntries)
                        .Select(s =>
                        {
                            var idx = s.IndexOf('=');
                            return idx >= 0
                                ? new KeyValuePair<string, string>(s.Substring(0, idx), s.Substring(idx + 1))
                                : new KeyValuePair<string, string>(s, string.Empty);
                        })
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
                    .AsReadOnly();
                }

                entry.Export = export;
                entry.ExportType = exportType;
                entry.ExportProperties = exportProperties;
                entry.ExportPropertiesContracts = exportPropertiesContracts;
                entry.ExportInterfaces = cc.ExportInterfaces;
                
                entries.Add(entry);
            }

            return conventionBuilder.ApplyConventions(entries);
        }

        public static ContainerConfiguration ApplyFactoriesFromCurrentConfig(this ContainerConfiguration containerConfiguration, string sectionName = "composition")
        {
            var section = (CompositionSection)ConfigurationManager.GetSection(sectionName);

            return ApplyFactories(containerConfiguration, section.Factories.Cast<CompositionFactoryElement>());
        }

        public static ContainerConfiguration ApplyFactories(this ContainerConfiguration containerConfiguration,
            System.Configuration.Configuration configuration, string sectionName = "composition")
        {
            var section = (CompositionSection)configuration.GetSection(sectionName);

            return ApplyFactories(containerConfiguration, section.Factories.Cast<CompositionFactoryElement>());
        }

        private static ContainerConfiguration ApplyFactories(ContainerConfiguration configuration, IEnumerable<CompositionFactoryElement> factories)
        {
            var entries = new List<FactoryEntry>();

            foreach (var fe in factories)
            {
                var entry = new FactoryEntry()
                {
                    Boundary = fe.Boundary,
                    ContractName = fe.ContractName,
                    Shared = fe.Shared
                };

                var factoryType = TryGetType(fe.FactoryType);
                var factoryMethod = factoryType.GetMethod(fe.FactoryMethod, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (factoryMethod == null)
                    throw new Exception(string.Format("Nie znaleziono metody {0}.", fe.FactoryMethod));

                entry.Factory = container => factoryMethod.Invoke(null, 
                    factoryMethod.GetParameters().Length > 0 ? factoryMethod.GetParameters().Select(pi => container.GetExport(pi.ParameterType)).ToArray() : null);
                entry.ExportType = factoryMethod.ReturnType;

                entries.Add(entry);
            }

            return configuration.ApplyFactories(entries);
        }

        private static Type TryGetType(string typeName)
        {
            var t = Type.GetType(typeName);
            if (t == null)
                throw new Exception(string.Format("Nie znaleziono typu: {0}.", typeName));
            return t;
        }
    }
}
