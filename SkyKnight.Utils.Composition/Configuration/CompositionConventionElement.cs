﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.Configuration
{
    public class CompositionConventionElement : ConfigurationElement
    {
        [ConfigurationProperty("forTypesDerivedFrom", IsRequired = false)]
        public string ForTypesDerivedFrom 
        {
            get { return (string)this["forTypesDerivedFrom"]; }
            set
            {
                this["forTypesDerivedFrom"] = value;
            }
        }

        [ConfigurationProperty("forType", IsRequired = false)]
        public string ForType
        {
            get { return (string)this["forType"]; }
            set
            {
                this["forType"] = value;
            }
        }

        [ConfigurationProperty("export", IsRequired = false)]
        public string Export
        {
            get { return (string)this["export"]; }
            set
            {
                this["export"] = value;
            }
        }

        [ConfigurationProperty("exportProperties", IsRequired = false)]
        public string ExportProperties
        {
            get { return (string)this["exportProperties"]; }
            set
            {
                this["exportProperties"] = value;
            }
        }

        [ConfigurationProperty("exportInterfaces", IsRequired = false)]
        public bool ExportInterfaces
        {
            get { return (bool)this["exportInterfaces"]; }
            set
            {
                this["exportInterfaces"] = value;
            }
        }

        [ConfigurationProperty("shared", IsRequired = false)]
        public bool Shared
        {
            get { return (bool)this["shared"]; }
            set
            {
                this["shared"] = value;
            }
        }

        [ConfigurationProperty("boundary", IsRequired = false)]
        public string Boundary
        {
            get { return (string)this["boundary"]; }
            set
            {
                this["boundary"] = value;
            }
        }

        // nazwy po przecinku
        [ConfigurationProperty("constructorContracts", IsRequired = false)]
        public string ConstructorContracts
        {
            get { return (string)this["constructorContracts"]; }
            set
            {
                this["constructorContracts"] = value;
            }
        }

        [ConfigurationProperty("constructorMatching", IsRequired = false, DefaultValue="default")]
        public string ConstructorMatching
        {
            get { return (string)this["constructorMatching"]; }
            set
            {
                this["constructorMatching"] = value;
            }
        }

        [ConfigurationProperty("contractName", IsRequired = false)]
        public string ContractName
        {
            get { return (string)this["contractName"]; }
            set
            {
                this["contractName"] = value;
            }
        }

        [ConfigurationProperty("metadata")]
        public CompositionMetadataElementCollection Metadata
        {
            get { return (CompositionMetadataElementCollection)this["metadata"]; }
            set { this["metadata"] = value; }
        }

        public string Key
        {
            get { return string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}", 
                ForTypesDerivedFrom, ForType, Export, Shared, Boundary, ConstructorContracts, ContractName); }
        }
    }
}
