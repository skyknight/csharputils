﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;

namespace SkyKnight.Utils.Composition
{
    public static class ContainerConfigurationExtensions
    {
        public static ContainerConfiguration WithExport<T>(this ContainerConfiguration configuration, T exportedInstance, string contractName = null, IDictionary<string, object> metadata = null)
        {
            return WithExport(configuration, exportedInstance, typeof(T), contractName, metadata);
        }

        public static ContainerConfiguration WithExport(this ContainerConfiguration configuration, object exportedInstance, Type contractType, string contractName = null, IDictionary<string, object> metadata = null)
        {
            return configuration.WithProvider(new InstanceExportDescriptorProvider(
                exportedInstance, contractType, contractName, metadata));
        }

        public static ContainerConfiguration WithFactoryDelegate<T>(this ContainerConfiguration configuration, Func<T> exportedInstanceFactory, string contractName = null, IDictionary<string, object> metadata = null, bool isShared = false, string boundaryName = null)
        {
            return WithFactoryDelegate(configuration, () => exportedInstanceFactory(), typeof(T), contractName, metadata, isShared, boundaryName);
        }

        public static ContainerConfiguration WithFactoryDelegate(this ContainerConfiguration configuration, Func<object> exportedInstanceFactory, Type contractType, string contractName = null, IDictionary<string, object> metadata = null, bool isShared = false, string boundaryName = null)
        {
            return configuration.WithProvider(new DelegateExportDescriptorProvider(
                c => exportedInstanceFactory(), contractType, contractName, metadata, isShared, boundaryName));
        }

        public static ContainerConfiguration WithFactoryDelegate<T>(this ContainerConfiguration configuration, Func<CompositionContext, T> exportedInstanceFactory, string contractName = null, IDictionary<string, object> metadata = null, bool isShared = false, string boundaryName = null)
        {
            return WithFactoryDelegate(configuration, c => exportedInstanceFactory(c), typeof(T), contractName, metadata, isShared, boundaryName);
        }

        public static ContainerConfiguration WithFactoryDelegate(this ContainerConfiguration configuration, Func<CompositionContext, object> exportedInstanceFactory, Type contractType, string contractName = null, IDictionary<string, object> metadata = null, bool isShared = false, string boundaryName = null)
        {
            return configuration.WithProvider(new DelegateExportDescriptorProvider(
                exportedInstanceFactory, contractType, contractName, metadata, isShared, boundaryName));
        }
    }
}