﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition
{
    public class ExtendedConventionBuilder : ConventionBuilder
    {
        public override IEnumerable<Attribute> GetCustomAttributes(Type reflectedType, MemberInfo member)
        {
            var result = base.GetCustomAttributes(reflectedType, member);

            if ((result == null || !result.Any()) && reflectedType != null && reflectedType.GetTypeInfo().IsGenericType && reflectedType.IsConstructedGenericType && member is ConstructorInfo)
            {
                var openType = reflectedType.GetGenericTypeDefinition();
                result = base.GetCustomAttributes(openType, FindOpenConstructor(openType, member as ConstructorInfo));
            }

            return result;
        }

        public override IEnumerable<Attribute> GetCustomAttributes(Type reflectedType, ParameterInfo parameter)
        {
            var result = base.GetCustomAttributes(reflectedType, parameter);

            if ((result == null || !result.Any()) && reflectedType != null && reflectedType.GetTypeInfo().IsGenericType && reflectedType.IsConstructedGenericType && parameter.Member is ConstructorInfo)
            {
                var openType = reflectedType.GetGenericTypeDefinition();
                var openConstructor = FindOpenConstructor(openType, parameter.Member as ConstructorInfo);
                var openConstructoParam = openConstructor.GetParameters().Single(p => p.Name == parameter.Name);
                result = base.GetCustomAttributes(openType, openConstructoParam);
            }

            return result;
        }

        private ConstructorInfo FindOpenConstructor(Type openType, ConstructorInfo closedConstructor)
        {
            var closedConstructorParams = closedConstructor.GetParameters();
#if PORTABLE
            var constructorQuery = openType.GetTypeInfo().DeclaredConstructors.Where(c => c.GetParameters().Length == closedConstructorParams.Length);
#else
            var constructorQuery = openType.GetConstructors().Where(c => c.GetParameters().Length == closedConstructorParams.Length);
#endif
            // @TODO: match parameters types
            var openConstructor = constructorQuery.Single();
            return openConstructor;
        }
    }
}
