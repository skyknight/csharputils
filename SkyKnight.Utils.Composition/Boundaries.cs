﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition
{
    public static class Boundaries
    {
        public const string Request = "Request";
        public const string Hub = "Hub";
        public const string Thread = "Thread";
    }
}
