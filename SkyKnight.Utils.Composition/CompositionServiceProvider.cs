﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition
{
    public class CompositionServiceProvider : IServiceProvider
    {
        private CompositionContext _compositionContext;

        public event EventHandler<Exception> OnServiceRetrievingError = delegate { };

        public CompositionServiceProvider(CompositionContext compositionContext)
        {
            _compositionContext = compositionContext;
        }

        public object GetService(Type serviceType)
        {
            object result = null;
            try
            {
                Type manyType = null;
                Type itemType = null;

                // @TODO: for now, GetExports returns only IEnumerable<>. If requested service is IList<> or ICollection<> it should return matching type.
                if (IsSupportedImportManyType(serviceType.GetTypeInfo(), out itemType, out manyType))
                    result = _compositionContext.GetExports(itemType);
                else
                    result = _compositionContext.GetExports(serviceType)?.LastOrDefault();
            }
            catch(Exception ex)
            {
                OnServiceRetrievingError(this, ex);
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return result;
        }

        static readonly Type[] SupportedImportManyTypes = new[] { typeof(IList<>), typeof(ICollection<>), typeof(IEnumerable<>) };

        bool IsSupportedImportManyType(TypeInfo typeInfo, out Type itemType, out Type manyType)
        {
            if(typeInfo.IsArray)
            {
                itemType = typeInfo.GetElementType();
                manyType = typeof(IEnumerable<>);
                return true;
            }

            if((typeInfo.IsGenericTypeDefinition && SupportedImportManyTypes.Contains(typeInfo.AsType())) ||
                (typeInfo.AsType().IsConstructedGenericType && SupportedImportManyTypes.Contains(typeInfo.GetGenericTypeDefinition())))
            {
                itemType = typeInfo.GenericTypeArguments.Single();
                manyType = typeInfo.GetGenericTypeDefinition();
                return true;
            }

            itemType = null;
            manyType = null;

            return false;
        }
    }
}
