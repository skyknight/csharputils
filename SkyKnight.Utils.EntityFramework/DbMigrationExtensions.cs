﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework
{
    public static class DbMigrationExtensions
    {
        // http://stackoverflow.com/questions/12418144/is-there-a-good-way-to-extend-the-code-first-migrations
        public static void CallSql(this DbMigration dbMigration, string query)
        {
            var methodInfo = typeof(DbMigration).GetMethod("Sql", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo.Invoke(dbMigration, new object[] { query, false, null });
        }

        public static void CallSql(this DbMigration dbMigration, IEnumerable<string> queries)
        {
            foreach (var query in queries)
            {
                dbMigration.CallSql(query);
            }
        }

        public static string GetSequenceNextValueStoredProcedureName(this DbMigration dbMigration, string seqName)
        {
            return string.Format(Const.SequenceNextValueStoredProcedureFormat, seqName);
        }
    }
}
