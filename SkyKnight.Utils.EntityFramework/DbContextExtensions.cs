﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework
{
    public static class DbContextExtensions
    {
        // http://weblogs.asp.net/ricardoperes/archive/2012/03/19/entity-framework-code-first-get-entities-from-local-cache-or-the-database.aspx
        public static IQueryable<T> LocalOrDatabase<T>(this DbContext context, Expression<Func<T, Boolean>> expression) where T : class
        {
            IEnumerable<T> localResults = context.Set<T>().Local.Where(expression.Compile());
            if (localResults.Any() == true)
                return (localResults.AsQueryable());

            IQueryable<T> databaseResults = context.Set<T>().Where(expression);
            return (databaseResults);
        }

        public static IQueryable<T> LocalOrDatabase<T>(this IDbSet<T> set, Expression<Func<T, Boolean>> expression) where T : class
        {
            IEnumerable<T> localResults = set.Local.Where(expression.Compile());
            if (localResults.Any() == true)
                return (localResults.AsQueryable());

            IQueryable<T> databaseResults = set.Where(expression);
            return (databaseResults);
        }

        //http://stackoverflow.com/questions/7253943/entity-framework-code-first-find-primary-key
        public static string GetPrimaryKeyProperty<TEntity>(this DbContext context) where TEntity : class
        {
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;
            var set = objectContext.CreateObjectSet<TEntity>();
            var keyNames = set.EntitySet.ElementType
                                                        .KeyMembers
                                                        .Select(k => k.Name);
            return keyNames.FirstOrDefault();
        }

        //http://stackoverflow.com/questions/5949623/entity-framework-code-first-get-name-of-created-table
        public static string GetTableName<T>(this DbContext context) where T : class
        {
            var type = typeof(T);
            var entityName = string.Empty;
            try
            {
                entityName =
                    (context as IObjectContextAdapter).ObjectContext
                        .CreateObjectSet<T>()
                        .EntitySet.Name;
            }
            catch
            {
            }
            var tableAttribute = type.GetCustomAttributes(false).OfType<System.ComponentModel.DataAnnotations.Schema.TableAttribute>().FirstOrDefault();

            return tableAttribute == null ? entityName : tableAttribute.Name;
        }

        //public static async Task<object> ExecuteProcedureScalarAsync(this DbContext dbContext)
        //{
        //    return null;
        //}

        //public static object ExecuteProcedureScalar(this DbContext dbContext, string procName)
        //{
        //    dbContext.Database.ExecuteSqlCommandAsync()
        //}
        public static T GetNextValue<T>(this DbContext dbContext, string seqName) where T : struct,
          IComparable,
          IComparable<T>,
          IConvertible,
          IEquatable<T>,
          IFormattable
        {
            var result = dbContext.Database.SqlQuery<Sequence<T>>(dbContext.GetSequenceNextValueStoredProcedureName(seqName));
            return result.First().NextValue;
        }

        private class Sequence<T> where T : struct,
          IComparable,
          IComparable<T>,
          IConvertible,
          IEquatable<T>,
          IFormattable
        {
            public T NextValue { get; set; }
        }

        public static string GetSequenceNextValueStoredProcedureName(this DbContext dbContext, string seqName)
        {
            return string.Format(Const.SequenceNextValueStoredProcedureFormat, seqName);
        }
    }
}
