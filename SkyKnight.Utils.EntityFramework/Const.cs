﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework
{
    class Const
    {
        public const string SequenceNextValueStoredProcedureFormat = "GetNextValueFor{0}";
    }
}
