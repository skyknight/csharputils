﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SkyKnight.Utils.Tests
{
    public class Lists
    {
        [Fact]
        public void ShouldBeSameList()
        {
            var list = new List<string>()
            {
                "first",
                "second",
                "third",
                "fourth"
            };

            var list2 = list.RemoveNearDuplicates();

            Assert.Equal(list, list2);
        }

        [Fact]
        public void ShouldBeShorter()
        {
            var list = new List<string>()
            {
                "first",
                "second",
                "second",
                "second",
                "third",
                "fourth",
                "fourth",
                "fifth",
                "sixth",
                "sixth"
            };

            var list2 = list.RemoveNearDuplicates();

            Assert.Equal(list2, new List<string>()
                {
                "first",
                "second",
                "third",
                "fourth",
                "fifth",
                "sixth"
                });
        }
    }
}
