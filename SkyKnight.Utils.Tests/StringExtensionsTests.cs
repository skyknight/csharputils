﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyKnight.Utils;
using Xunit;

namespace SkyKnight.Utils.Tests
{
    public class StringExtensionsTests
    {
        private IDictionary<string, IEnumerable<Version>> BuildStringsToFindVersions()
        {
            var data = new Dictionary<string, IEnumerable<Version>>()
            {
                { @"Microsoft SQL Server 2014 - 12.0.2269.0 (X64) 
    Jun 10 2015 03:35:45
    Copyright(c) Microsoft Corporation
    Express Edition (64 - bit) on Windows NT 6.3 < X64 > (Build 10240: )
", new[] { new Version("12.0.2269.0"), new Version("6.3") } },
                { @"Microsoft SQL Server 2012 - 11.0.2218.0 (X64) 
	Jun 12 2012 13:05:25 
	Copyright (c) Microsoft Corporation
	Express Edition (64-bit) on Windows NT 6.2 <X64> (Build 9200: ) (Hypervisor)
", new[] { new Version("11.0.2218.0"), new Version("6.2") } }
            };

            return data;
        }

        [Fact]
        public void Should_Find_Two_Versions()
        {
            var data = BuildStringsToFindVersions();

            foreach (var item in data)
            {
                Assert.Equal(item.Key.FindVersions(), item.Value);
            }
        }
    }
}
