﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SkyKnight.Utils.Composition.Mvc
{
    public static class ControllerBuilderExtensions
    {
        public static void SetCompositionControllerFactory(this ControllerBuilder controllerBuilder)
        {
            controllerBuilder.SetControllerFactory(new CompositionControllerFactory());
        }
    }
}
