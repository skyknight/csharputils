﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SkyKnight.Utils.Composition.Mvc
{
    public class CompositionControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            var owinContext = requestContext.HttpContext.GetOwinContext();
            if (owinContext != null)
            {
                var scopeContainer = owinContext.Get<CompositionContext>("HttpRequestScope");
                if (scopeContainer != null)
                {
                    var controller = scopeContainer.GetExport(controllerType) as IController;
                    if (controller != null)
                        return controller;
                }
            }
            return base.GetControllerInstance(requestContext, controllerType);
        }
    }
}
