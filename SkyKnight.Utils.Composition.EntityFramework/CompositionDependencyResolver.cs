﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.EntityFramework
{
    public class CompositionDependencyResolver : IDbDependencyResolver
    {
        private CompositionContext _context;

        public CompositionDependencyResolver(CompositionContext context)
        {
            _context = context;
        }

        public object GetService(Type type, object key)
        {
            return _context.GetExports(type).FirstOrDefault();
        }

        public IEnumerable<object> GetServices(Type type, object key)
        {
            return _context.GetExports(type);
        }
    }
}
