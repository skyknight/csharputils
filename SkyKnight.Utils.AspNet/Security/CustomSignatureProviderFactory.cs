﻿using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace SkyKnight.Utils.AspNet.Security
{
#if DNX451
    // https://gist.github.com/sunsided/b57cdb487442c68a53cb
    // https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/issues/324
    // https://github.com/AzureAD/azure-activedirectory-identitymodel-extensions-for-dotnet/issues/179#issuecomment-155972405
    public class CustomSignatureProviderFactory : SignatureProviderFactory
    {
        public override Microsoft.IdentityModel.Tokens.SignatureProvider CreateForSigning(SecurityKey key, string algorithm)
        {
            var asymmetricKey = key as AsymmetricSecurityKey;
            return asymmetricKey == null
                ? base.CreateForSigning(key, algorithm)
                : new CustomAsymmetricSignatureProvider(asymmetricKey, algorithm, willCreateSignatures: true);
        }

        public override Microsoft.IdentityModel.Tokens.SignatureProvider CreateForVerifying(SecurityKey key, string algorithm)
        {
            var asymmetricKey = key as AsymmetricSecurityKey;
            return asymmetricKey == null
                ? base.CreateForVerifying(key, algorithm)
                : new CustomAsymmetricSignatureProvider(asymmetricKey, algorithm, willCreateSignatures: false);
        }

        public override void ReleaseProvider(Microsoft.IdentityModel.Tokens.SignatureProvider signatureProvider)
        {
            base.ReleaseProvider(signatureProvider);
        }
    }
#endif
}