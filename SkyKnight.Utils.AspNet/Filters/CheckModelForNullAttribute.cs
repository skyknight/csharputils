﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#if DNX451 || DNXCORE50
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;
#else
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
#endif

#if DNX451 || DNXCORE50
namespace SkyKnight.Utils.AspNet.Filters
#else
namespace SkyKnight.Utils.WebApi.Filters
#endif
{
    // http://www.strathweb.com/2012/10/clean-up-your-web-api-controllers-with-model-validation-and-null-check-filters/
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class CheckModelForNullAttribute : ActionFilterAttribute
    {
        private readonly Func<IDictionary<string, object>, bool> _validate;

        public CheckModelForNullAttribute()
            : this(arguments =>
                arguments.Any(kv => kv.Value == null))
        { }

        public CheckModelForNullAttribute(Func<IDictionary<string, object>, bool> checkCondition)
        {
            _validate = checkCondition;
        }

#if DNX451 || DNXCORE50
        public override void OnActionExecuting(ActionExecutingContext actionContext)
#else
        public override void OnActionExecuting(HttpActionContext actionContext)
#endif
        {
            if (_validate(actionContext.ActionArguments))
            {
#if DNX451 || DNXCORE50
                actionContext.Result = new BadRequestObjectResult("The argument cannot be null");
#else
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, "The argument cannot be null");
#endif
            }
        }
    }
}
