﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#if DNX451 || DNXCORE50
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Filters;
#else
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
#endif

#if DNX451 || DNXCORE50
namespace SkyKnight.Utils.AspNet.Filters
#else
namespace SkyKnight.Utils.WebApi.Filters
#endif
{
    // http://www.asp.net/web-api/overview/formats-and-model-binding/model-validation-in-aspnet-web-api
    public class ValidateModelAttribute : ActionFilterAttribute
    {
#if DNX451 || DNXCORE50
        public override void OnActionExecuting(ActionExecutingContext actionContext)
#else
        public override void OnActionExecuting(HttpActionContext actionContext)
#endif
        {
            if (!actionContext.ModelState.IsValid)
            {
#if DNX451 || DNXCORE50
                actionContext.Result = new BadRequestObjectResult(actionContext.ModelState);
#else
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, actionContext.ModelState);
#endif
            }
        }
    }
}
