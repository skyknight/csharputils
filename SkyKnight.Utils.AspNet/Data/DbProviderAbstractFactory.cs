﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SkyKnight.Utils.AspNet.Data
{
    public class DbProviderAbstractFactory
    {
        private Dictionary<string, DbProviderFactory> _factories;
#if DNX451
        private bool _fallbackToDbProviderFactories;

        public DbProviderAbstractFactory(IConfiguration configuration, bool fallbackToDbProviderFactories = false)
            : this(BuildFactories(configuration), fallbackToDbProviderFactories)
        { }

        public DbProviderAbstractFactory(IDictionary<string, string> factoriesTypes, bool fallbackToDbProviderFactories = false)
            : this(BuildFactories(factoriesTypes), fallbackToDbProviderFactories)
        { }

        public DbProviderAbstractFactory(IDictionary<string, Type> factoriesTypes, bool fallbackToDbProviderFactories = false)
            : this(BuildFactories(factoriesTypes), fallbackToDbProviderFactories)
        { }

        public DbProviderAbstractFactory(IDictionary<string, DbProviderFactory> factories, bool fallbackToDbProviderFactories = false)
        {
            _factories = new Dictionary<string, DbProviderFactory>(factories);
            _fallbackToDbProviderFactories = fallbackToDbProviderFactories;
        }
#else
        public DbProviderAbstractFactory(IConfiguration configuration)
            : this(BuildFactories(configuration))
        { }

        public DbProviderAbstractFactory(IDictionary<string, string> factoriesTypes)
            : this(BuildFactories(factoriesTypes))
        { }

        public DbProviderAbstractFactory(IDictionary<string, Type> factoriesTypes)
            : this(BuildFactories(factoriesTypes))
        { }

        public DbProviderAbstractFactory(IDictionary<string, DbProviderFactory> factories)
        {
            _factories = new Dictionary<string, DbProviderFactory>(factories);
        }
#endif

        public DbProviderFactory GetFactory(string dbProvider)
        {
            //System.Data.SqlClient.SqlClientFactory.Instance
            DbProviderFactory factory;
            if(!_factories.TryGetValue(dbProvider, out factory))
            {
#if DNX451
                if (_fallbackToDbProviderFactories)
                    return DbProviderFactories.GetFactory(dbProvider);
#endif
                throw new Exception(string.Format("Cannot find {0} provider.", dbProvider));
            }
            return factory;
        }

        private static IDictionary<string, DbProviderFactory> BuildFactories(IDictionary<string, Type> factoriesTypes)
        {
            var factories = new Dictionary<string, DbProviderFactory>();
            foreach (var ft in factoriesTypes)
            {
                var instanceProperty = ft.Value.GetField("Instance", BindingFlags.Static | BindingFlags.Public);
                if (instanceProperty == null)
                    throw new Exception(string.Format("Invalid provider factory type for {0}.", ft.Key));
                var factory = instanceProperty.GetValue(null);
                if (factory == null)
                    throw new Exception(string.Format("Provider factory cannot be null: {0}.", ft.Key));
                factories.Add(ft.Key, (DbProviderFactory)factory);
            }
            return factories;
        }

        private static IDictionary<string, DbProviderFactory> BuildFactories(IDictionary<string, string> providers)
        {
            var factoriesTypes = new Dictionary<string, Type>();

            foreach (var provider in providers)
            {
                //var factoryTypeName = provider.Key + provider.Key.Substring(provider.Key.LastIndexOf('.')) + "Factory";
                var factoryType = Type.GetType(provider.Value);// Assembly.Load(new AssemblyName(provider.Value))?.GetType(factoryTypeName);
                if (factoryType == null)
                    throw new Exception(string.Format("Cannot find type for provider: {0}.", provider.Key));
                factoriesTypes.Add(provider.Key, factoryType);
            }

            return BuildFactories(factoriesTypes);
        }

        private static IDictionary<string, DbProviderFactory> BuildFactories(IConfiguration configuration)
        {
            var dbProvidersSection = configuration.GetSection("Data:DbProviderFactories");
            if (dbProvidersSection == null)
                return new Dictionary<string, DbProviderFactory>();

            var factoriesTypes = new Dictionary<string, string>();
            foreach (var dbProvider in dbProvidersSection.GetChildren())
            {
                factoriesTypes.Add(dbProvider.Key, dbProvider.Value);
            }
            return BuildFactories(factoriesTypes);
        }
    }
}
