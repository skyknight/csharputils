﻿using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;

namespace SkyKnight.Utils.AspNet.Auth
{
    public abstract class TokenControllerBase<TUser> : Controller where TUser : class
    {
        private TokenAuthOptions _tokenOptions;

        protected TokenControllerBase(TokenAuthOptions tokenOptions)
        {
            _tokenOptions = tokenOptions;
        }

        /// <summary>
        /// Check if currently authenticated. Will throw an exception of some sort which shoudl be caught by a general
        /// exception handler and returned to the user as a 401, if not authenticated. Will return a fresh token if
        /// the user is authenticated, which will reset the expiry.
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize("Bearer")]
        public dynamic Get()
        {
            bool authenticated = false;
            string user = null;
            string token = null;
            DateTime? tokenExpires = default(DateTime?);

            var currentUser = HttpContext.User;
            if (currentUser != null)
            {
                authenticated = currentUser.Identity.IsAuthenticated;
                if (authenticated)
                {
                    user = currentUser.Identity.Name;
                    tokenExpires = DateTime.UtcNow.Add(_tokenOptions.AccessTokenExpireTimeSpan);
                    token = GetToken(new ClaimsIdentity(currentUser.Identity, currentUser.Claims), tokenExpires);
                }
            }
            
            return new { authenticated = authenticated, user = user, token = token, tokenExpires = tokenExpires };
        }

        /// <summary>
        /// Request a new token for a given username/password pair.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic Post([FromBody] AuthRequest req)
        {
            TUser user = null;
            // Obviously, at this point you need to validate the username and password against whatever system you wish.
            if (req != null && Authenticate(req, out user))
            {
                DateTime? expires = DateTime.UtcNow.Add(_tokenOptions.AccessTokenExpireTimeSpan);
                var token = GetToken(user, expires);
                return new { authenticated = true, user = GetUserName(user), token = token, tokenExpires = expires };
            }
            return new { authenticated = false };
        }

        private string GetToken(TUser user, DateTime? expires)
        {
            var handler = new JwtSecurityTokenHandler();

            ClaimsIdentity identity = new ClaimsIdentity(new GenericIdentity(GetUserName(user), "TokenAuth"), CreateClaims(user));

            var securityToken = handler.CreateToken(
                issuer: _tokenOptions.Issuer,
                audience: _tokenOptions.Audience,
                signingCredentials: _tokenOptions.SigningCredentials,
                subject: identity,
                expires: expires
                );
            return handler.WriteToken(securityToken);
        }

        private string GetToken(ClaimsIdentity identity, DateTime? expires)
        {
            var handler = new JwtSecurityTokenHandler();

            var securityToken = handler.CreateToken(
                issuer: _tokenOptions.Issuer,
                audience: _tokenOptions.Audience,
                signingCredentials: _tokenOptions.SigningCredentials,
                subject: identity,
                expires: expires
                );
            return handler.WriteToken(securityToken);
        }

        protected abstract bool Authenticate(AuthRequest authRequst, out TUser user);
        protected abstract string GetUserName(TUser user);

        protected virtual IEnumerable<Claim> CreateClaims(TUser user)
        {
            return null;
        }
    }
}
