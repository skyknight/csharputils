﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetApplicationExample
{
    public class ChatHub : Hub
    {
        public void Send(string message)
        {
            Clients.All.send(message);
        }
    }
}
