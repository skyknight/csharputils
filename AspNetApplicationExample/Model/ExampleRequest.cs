﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetApplicationExample.Model
{
    public class ExampleRequest
    {
        [Required]
        public string Name { get; set; }

        public int Quantity { get; set; }
    }
}
