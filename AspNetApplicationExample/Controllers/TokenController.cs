﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using SkyKnight.Utils.AspNet.Auth;
using AspNetApplicationExample.Model;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AspNetApplicationExample.Controllers
{
    [Route("Token")]
    public class TokenController : TokenControllerBase<AppUser>
    {
        public TokenController(TokenAuthOptions tokenOptions)
    : base(tokenOptions)
        {
        }

        protected override bool Authenticate(AuthRequest authRequst, out AppUser user)
        {
            user = new AppUser()
            {
                Id = 1,
                UserName = "skywalker"
            };
            return true;
        }

        protected override string GetUserName(AppUser user)
        {
            return user.UserName;
        }
    }
}
