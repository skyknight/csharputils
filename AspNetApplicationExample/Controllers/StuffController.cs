﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using SkyKnight.Utils.AspNet.Filters;
using AspNetApplicationExample.Model;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AspNetApplicationExample.Controllers
{
    [Route("api/[controller]/[action]")]
    public class StuffController : Controller
    {
        public int GetSomething()
        {
            return 2;
        }

        [HttpPost, ValidateModel, CheckModelForNull]
        public int SaveSomething([FromBody]ExampleRequest request)
        {
            return 1;
        }
    }
}
