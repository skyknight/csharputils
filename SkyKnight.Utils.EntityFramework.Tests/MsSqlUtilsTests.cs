﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SkyKnight.Utils.EntityFramework.Tests
{
    public class MsSqlUtilsTests
    {
        [Fact]
        public void Database_Parse_Objects()
        {
            var data = new Dictionary<string, Tuple<string, string>>()
            {
                { "[dbo].obiekt", new Tuple<string, string>("dbo", "obiekt") },
                { "[dbo].[InnyObiektt", new Tuple<string, string>("dbo", "InnyObiektt") },
                { "dbo.blabla", new Tuple<string, string>("dbo", "blabla") },
                { "sekwencja", new Tuple<string, string>(string.Empty, "sekwencja") },
                { "[objekt]", new Tuple<string, string>(string.Empty, "objekt") },
                { "dbo.[yyy]", new Tuple<string, string>("dbo", "yyy") },
                { "[test].[tabela]", new Tuple<string, string>("test", "tabela") }
            };

            foreach (var item in data)
            {
                string schemaName = null;
                string objectName = null;

                var result = MsSql.SqlUtils.TryParseDatabaseObject(item.Key, out schemaName, out objectName);
                Assert.True(result);
                Assert.Equal(schemaName, item.Value.Item1);
                Assert.Equal(objectName, item.Value.Item2);
            }
        }
    }
}
