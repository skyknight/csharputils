﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public class SignalRDependencyResolver : DefaultDependencyResolver
    {
        private readonly CompositionContext _container;

        public SignalRDependencyResolver(CompositionContext container)
            : base()
        {
            _container = container;
        }

        public override object GetService(Type serviceType)
        {
            var export = _container.GetExports(serviceType, null).SingleOrDefault();

            var r = null != export ? export : base.GetService(serviceType);
            return r;
        }

        public override IEnumerable<object> GetServices(Type serviceType)
        {
            var r = base.GetServices(serviceType);

            var exports = _container.GetExports(serviceType, null);
            IEnumerable<object> createdObjects = new List<object>();

            if (exports.Any())
                createdObjects = createdObjects.Concat(exports);

            return createdObjects.Concat(r);
        }
    }
}
