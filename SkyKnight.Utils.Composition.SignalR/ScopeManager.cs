﻿using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public class ScopeManager : IScopeManager
    {
        readonly ConcurrentDictionary<IHub, Export<CompositionContext>> _hubLifetimeScopes =
            new ConcurrentDictionary<IHub, Export<CompositionContext>>();

        readonly ExportFactory<CompositionContext> _requestScopeFactory;

        public ScopeManager(ExportFactory<CompositionContext> requestScopeFactory)
        {
            _requestScopeFactory = requestScopeFactory;
        }

        public T CreateHub<T>(Type hubType) where T : BaseHub
        {
            var scope = _requestScopeFactory.CreateExport();
            var hub = scope.Value.GetExport(hubType) as T;
            hub.Disposing += HubOnDisposing;
            _hubLifetimeScopes.TryAdd(hub, scope);
            return hub;
        }

        void HubOnDisposing(object sender, EventArgs eventArgs)
        {
            var hub = sender as BaseHub;
            if (hub == null)
                return;
            hub.Disposing -= HubOnDisposing;
            Export<CompositionContext> scope;
            if (!_hubLifetimeScopes.TryRemove(hub, out scope))
                return;
            scope.Dispose();
        }

        public CompositionContext TryGetHubScope(BaseHub hub)
        {
            Export<CompositionContext> scope = null;
            _hubLifetimeScopes.TryGetValue(hub, out scope);
            return scope != null ? scope.Value : null;
        }
    }
}