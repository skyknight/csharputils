﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public abstract class BaseHub : Hub
    {
        public EventHandler Disposing;
        bool _disposed;
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            _disposed = true;

            base.Dispose(disposing);
            if (!disposing)
                return;

            var handler = Disposing;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
