﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public interface IScopeManager
    {
        T CreateHub<T>(Type hubType) where T : BaseHub;
        CompositionContext TryGetHubScope(BaseHub hub);
    }
}
