﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public class HubActivator : IHubActivator
    {
        private readonly IDependencyResolver _resolver;
        private readonly IScopeManager _scopeManager;

        public HubActivator(IDependencyResolver resolver, IScopeManager scopeManager)
        {
            _resolver = resolver;
            _scopeManager = scopeManager;
        }

        public IHub Create(HubDescriptor descriptor)
        {
            if (descriptor == null)
                throw new ArgumentNullException("descriptor");

            if (descriptor.HubType == null)
                return null;

            return typeof(BaseHub).IsAssignableFrom(descriptor.HubType)
                ? _scopeManager.CreateHub<BaseHub>(descriptor.HubType)
                : (_resolver.Resolve(descriptor.HubType) as IHub ?? (IHub)Activator.CreateInstance(descriptor.HubType));
        }
    }
}
