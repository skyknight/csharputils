﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.SignalR
{
    public static class ConventionBuilderExtensions
    {
        public class Factories
        {
            public static string BoundaryName { get; set; }

            public Factories(CompositionContext compositionHost)
            {
                CompositionHost = compositionHost;
            }

            public CompositionContext CompositionHost { get; private set; }

            public ExportFactory<CompositionContext> Scope
            {
                get
                {
                    return CompositionHost.CreateScopeFactory(BoundaryName);
                }
            }
        }

        public static void ApplySignalRConventions(this ConventionBuilder conventions, string boundaryName = "Hub")
        {
            Factories.BoundaryName = boundaryName;

            conventions
                .ForType<Factories>()
                .ExportProperty<ExportFactory<CompositionContext>>(ps => ps.Scope, ecb => ecb.AsContractName("HubScope"))
                .Shared();

            conventions.ForTypesDerivedFrom<Microsoft.AspNet.SignalR.Hubs.IHub>()
                .Export();

            conventions.ForType<HubActivator>()
                .Export<Microsoft.AspNet.SignalR.Hubs.IHubActivator>()
                .Shared();

            conventions.ForType<ScopeManager>()
                .SelectConstructor(cs => cs.First(), (pi, ic) => ic.AsContractName("HubScope"))
                .Export<IScopeManager>()
                .Shared();

            conventions.ForType<SignalRDependencyResolver>()
                .Export<Microsoft.AspNet.SignalR.IDependencyResolver>()
                .Shared();

            conventions.ForType<Microsoft.AspNet.SignalR.Infrastructure.ConnectionManager>()
                .Export<Microsoft.AspNet.SignalR.Infrastructure.IConnectionManager>()
                .Shared();
        }
    }
}
