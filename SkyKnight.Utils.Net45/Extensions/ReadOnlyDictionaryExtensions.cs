﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils
{
    public static class ReadOnlyDictionaryExtensions
    {
        public static TValue TryGetValueOrDefault<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dic, TKey key)
        {
            TValue v = default(TValue);
            if (key == null)
                return v;
            dic.TryGetValue(key, out v);
            return v;
        }

        public static bool TryGetValue<TValue>(this IReadOnlyDictionary<string, object> dic, string key, out TValue value, Func<object, TValue> converter = null)
        {
            value = default(TValue);
            object v = null;
            if (dic.TryGetValue(key, out v))
            {
                if (v is TValue)
                    value = (TValue)v;
                else if (converter != null)
                    value = converter(v);
                else
                    throw new Exception(string.Format("Cannot convert from {0} to {1}.", v.GetType(), typeof(TValue)));
                return true;
            }
            return false;
        }

        // http://stackoverflow.com/questions/678379/is-there-a-read-only-generic-dictionary-available-in-net
        public static IReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(
    this IDictionary<TKey, TValue> dictionary)
        {
            return dictionary.With(x => new ReadOnlyDictionary<TKey, TValue>(dictionary));
        }
    }
}
