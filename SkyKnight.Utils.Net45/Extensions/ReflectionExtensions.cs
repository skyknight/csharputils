﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils
{
    public static class ReflectionExtensions
    {
        public static bool IsAsyncCall(this MethodInfo methodInfo)
        {
            return methodInfo.ReturnType != null && (methodInfo.ReturnType == typeof(Task) || methodInfo.ReturnType.IsSubclassOf(typeof(Task)));
        }
    }
}
