﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SkyKnight.Utils.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Owin
{
    public static class AppBuilderExtensions
    {
        public static IAppBuilder UseExtendedOAuthBearerTokens(this IAppBuilder app,
            OAuthAuthorizationServerOptions options)
        {
            App.AuthenticationOptions = options;
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
            {
                AccessTokenFormat = options.AccessTokenFormat,
                AccessTokenProvider = options.AccessTokenProvider,
                AuthenticationMode = options.AuthenticationMode,
                AuthenticationType = options.AuthenticationType,
                Description = options.Description,
                Provider = new ExtendedApplicationOAuthTokenProvider(req => req.Query.Get("bearer_token"),
                 req => req.Query.Get("access_token"),
                 req => req.Query.Get("token"),
                 req => req.Headers.Get("X-Token")),
                SystemClock = options.SystemClock
            });
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
            {
                AccessTokenFormat = options.AccessTokenFormat,
                AccessTokenProvider = options.AccessTokenProvider,
                AuthenticationMode = AuthenticationMode.Passive,
                AuthenticationType = "ExternalBearer",
                Description = options.Description,
                Provider = new ExternalOAuthBearerProvider(),
                SystemClock = options.SystemClock
            });
            return app;
        }

        private class ExternalOAuthBearerProvider : OAuthBearerAuthenticationProvider
        {
            public ExternalOAuthBearerProvider()
            {
            }

            public override Task ValidateIdentity(OAuthValidateIdentityContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException("context");
                }
                if (context.Ticket.Identity.Claims.Count<Claim>() == 0)
                {
                    context.Rejected();
                }
                else if (context.Ticket.Identity.Claims.All<Claim>((Claim c) => c.Issuer == "LOCAL AUTHORITY"))
                {
                    context.Rejected();
                }
                return Task.FromResult<object>(null);
            }
        }
    }
}
