﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Owin
{
    public class App
    {
        public static AuthenticationOptions AuthenticationOptions { get; internal set; }
    }
}
