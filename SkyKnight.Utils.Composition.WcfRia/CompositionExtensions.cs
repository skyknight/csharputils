﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.WcfRia
{
    public static class CompositionExtensions
    {
        public static void ApplyWcfRiaConvention(this ConventionBuilder conventionBuilder, string boundaryName = Boundaries.Request)
        {
            CompositionDomainServiceFactory.ScopeBoundary = boundaryName;
            conventionBuilder
                .ForType<CompositionDomainServiceFactory>()
                .Export<IDomainServiceFactory>()
                .Shared();
        }
    }
}
