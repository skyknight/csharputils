﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.WcfRia
{
    public class CompositionDomainServiceFactory : IDomainServiceFactory
    {
        public static string ScopeBoundary { get; set; }

        static CompositionDomainServiceFactory()
        {
            ScopeBoundary = Boundaries.Request;
        }

        readonly ExportFactory<CompositionContext> _requestScopeFactory;

        private ConcurrentDictionary<DomainService, Export<CompositionContext>> _containers
    = new ConcurrentDictionary<DomainService, Export<CompositionContext>>();

        public CompositionDomainServiceFactory(CompositionContext compositionContext)
        {
            _requestScopeFactory = compositionContext.CreateScopeFactory(ScopeBoundary);
        }

        public DomainService CreateDomainService(Type domainServiceType, DomainServiceContext context)
        {
            var container = _requestScopeFactory.CreateExport();

            var domainService = container.Value.GetExport(domainServiceType) as DomainService;
            domainService.Initialize(context);
            _containers.TryAdd(domainService, container);
            return domainService;
        }

        public void ReleaseDomainService(DomainService domainService)
        {
            Export<CompositionContext> container;
            if (_containers.TryRemove(domainService, out container))
                container.Dispose();
            domainService.Dispose();
        }
    }
}
