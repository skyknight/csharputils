﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFModelExample
{
    public class Order
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OrderId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public virtual List<OrderItem> Items { get; set; }

        public virtual List<OrderPerson> Persons { get; set; }
    }
}
