﻿using CodeFirstStoreFunctions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFModelExample
{
    public class ExampleDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public DbSet<OrderPerson> OrderPersons { get; set; }

        public ExampleDbContext()
            : base("ExampleDbContext")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder
                .Entity<Person>()
                .ToTable("PeopleView")
                .MapToStoredProcedures(s => s.Update(u => u.HasName("Person_Update")).Delete(d => d.HasName("Person_Delete")).Insert(i => i.HasName("Person_Insert")));
        }
    }
}
