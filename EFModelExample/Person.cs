﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFModelExample
{
    public class Person
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PersonId { get; set; }

        public string Name { get; set; }

        public DateTime? BirthDay { get; set; }

        public virtual List<OrderPerson> Orders { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int OrdersCount { get; set; }
    }
}
