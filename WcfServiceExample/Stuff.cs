﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfServiceExample
{
    public class Foo : IDisposable
    {
        public DateTime CreatedAt { get; private set; }

        public Foo()
        {
            CreatedAt = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Foo created.");
        }

        public void Dispose()
        {
            System.Diagnostics.Debug.WriteLine("Foo disposed.");
        }
    }

    public class Bar : IDisposable
    {
        public DateTime CreatedAt { get; private set; }

        public Bar()
        {
            CreatedAt = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Bar created.");
        }

        public void Dispose()
        {
            System.Diagnostics.Debug.WriteLine("Bar disposed.");
        }
    }
}