﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Web;
using SkyKnight.Utils.Composition.Wcf;
using SkyKnight.Utils.Composition;

namespace WcfServiceExample
{
    static class IoC
    {
        public static void ConfiugreCompositionContext()
        {
            var conventions = new ConventionBuilder();

            conventions.ForType<Foo>().Export().Shared();
            conventions.ForType<Bar>().Export().Shared(Boundaries.Request);
            conventions.ForType<Service1>().Export().Shared(Boundaries.Request);
            conventions.ApplyWcfConvenction();

            new ContainerConfiguration()
                .WithAssemblies(new[] { Assembly.GetExecutingAssembly() }.AddWcfCompositionAssemblies(), conventions)
                .CreateContainer()
                .ApplyToHostFactory();
        }
    }
}