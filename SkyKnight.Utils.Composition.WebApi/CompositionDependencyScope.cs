﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace SkyKnight.Utils.Composition.WebApi
{
    class CompositionDependencyScope : IDependencyScope
    {
        private readonly CompositionContext _compositionScope;

        public CompositionDependencyScope(CompositionContext compositionScope)
        {
            if (compositionScope == null)
                throw new ArgumentNullException("compositionScope");

            _compositionScope = compositionScope;
        }

        public void Dispose()
        {
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            object result;
            _compositionScope.TryGetExport(serviceType, null, out result);
            return result;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            return _compositionScope.GetExports(serviceType, null);
        }
    }
}
