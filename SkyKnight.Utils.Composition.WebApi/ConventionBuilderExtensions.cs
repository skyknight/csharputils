﻿using System;
using System.Collections.Generic;
using System.Composition.Convention;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.Composition.WebApi
{
    public static class ConventionBuilderExtensions
    {
        public static void ApplyWebApiConventions(this ConventionBuilder conventions)
        {
            conventions
                .ForTypesDerivedFrom<System.Web.Http.Controllers.IHttpController>()
                .Export();
        }
    }
}
