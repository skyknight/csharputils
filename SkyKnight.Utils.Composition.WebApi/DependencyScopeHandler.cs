﻿using SkyKnight.Utils.Composition.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Hosting;

namespace SkyKnight.Utils.Composition.WebApi
{
    public class DependencyScopeHandler : DelegatingHandler
    {
        [SecuritySafeCritical]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException("request");

            var owinContext = request.GetOwinContext();
            if (owinContext == null) return base.SendAsync(request, cancellationToken);

            var composition = owinContext.GetCompositionScope();
            if (composition == null) return base.SendAsync(request, cancellationToken);

            var dependencyScope = new CompositionDependencyScope(composition);            

            request.Properties[HttpPropertyKeys.DependencyScope] = dependencyScope;

            return base.SendAsync(request, cancellationToken);
        }
    }
}
