﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SkyKnight.Utils.Composition.WebApi
{
    public static class AppBuilderExtensions
    {
        public static IAppBuilder UseCompositionWebApi(this IAppBuilder app, HttpConfiguration configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            if (!configuration.MessageHandlers.OfType<DependencyScopeHandler>().Any())
                configuration.MessageHandlers.Insert(0, new DependencyScopeHandler());

            return app;
        }
    }
}
