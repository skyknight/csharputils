﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyKnight.Utils;

namespace SkyKnight.T4TS.Core
{
    static class DictionaryExtensions
    {
        public static TValue TryAddAndReturnValue<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            dic.TryAdd(key, value);
            return value;
        }
    }
}
