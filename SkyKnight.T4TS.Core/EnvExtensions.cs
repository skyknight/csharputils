﻿using EnvDTE;
using EnvDTE80;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    public static class EnvExtensions
    {
        public static void AddToQueue(this ProjectItems projectItems, Queue<ProjectItem> queue)
        {
            foreach (ProjectItem item in projectItems)
            {
                queue.Enqueue(item);
            }
        }

        public static void ForEachCodeClass(this Project project, Action<CodeClass> fnCodeClass)
        {
            Queue<ProjectItem> projectItems = new Queue<ProjectItem>();

            project.ProjectItems.AddToQueue(projectItems);

            ProjectItem pi;

            while (projectItems.Count > 0 && (pi = projectItems.Dequeue()) != null)
            {
                if (pi.FileCodeModel != null)
                {
                    var codeElements = pi.FileCodeModel.CodeElements;
                    foreach (var ns in codeElements.OfType<CodeNamespace>())
                    {
                        if (ns.Members != null)
                        {
                            foreach (var codeClass in ns.Members.OfType<CodeClass>())
                            {
                                fnCodeClass(codeClass);
                            }
                        }
                    }
                }

                if (pi.ProjectItems != null)
                    pi.ProjectItems.AddToQueue(projectItems);
            }
        }

        public static string GetUnwrappedTaskTypeName(this CodeTypeRef taskType)
        {
            var fn = taskType.AsFullName;
            if (fn.StartsWith(Consts.TaskVoidFullName))
                return Utils.UnwrapGenericType(fn);
            return fn;
        }

        public static bool TryGetAttribute(this CodeElements attributes, string attributeFullName, out CodeAttribute attribute)
        {
            foreach (CodeAttribute attr in attributes)
            {
                if (attr.FullName == attributeFullName)
                {
                    attribute = attr;
                    return true;
                }
            }

            attribute = null;
            return false;
        }

        public static Dictionary<string, string> GetAttributeValues(this CodeAttribute codeAttribute)
        {
            var values = new Dictionary<string, string>();
            foreach (CodeElement child in codeAttribute.Children)
            {
                var property = (EnvDTE80.CodeAttributeArgument)child;
                if (property == null || property.Value == null)
                    continue;

                // remove quotes if the property is a string
                string val = property.Value ?? string.Empty;
                if (val.StartsWith("\"") && val.EndsWith("\""))
                    val = val.Substring(1, val.Length - 2);

                values.Add(property.Name, val);
            }

            return values;
        }

        public static Project FindProject(this Solution solution, string projectName)
        {
            var projects = new List<Project>();
            projects.AddRange(solution.OfType<Project>());

            for (int i = 0; i < projects.Count; i++)
            {
                var item = projects[i];

                if (item == null)
                    continue;

                var pn = item.Name;
                SolutionFolder folder = null;

                try // no time for special cases :x
                {
                    folder = item.Object as SolutionFolder;
                }
                catch { }

                if (folder == null && pn.Equals(projectName, StringComparison.OrdinalIgnoreCase))
                    return item;

                if (folder != null)
                    projects.AddRange(item.ProjectItems.OfType<ProjectItem>().Select(pi => pi.Object as Project).ToArray());
            }

            return null;
        }
    }
}
