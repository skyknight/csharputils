﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;
using SkyKnight.T4TS.Core.Model;
using EnvDTE80;
using SkyKnight.Utils;

namespace SkyKnight.T4TS.Core
{
    public abstract class BaseRemoteServiceDiscoverer : IRemoteServiceDiscoverer
    {

        public RemoteServiceDiscovererResult DiscoverOperations(string serviceName, IEnumerable<Project> projects)
        {
            var rsClasses = new List<CodeClass>();
            var classesCache = new Dictionary<string, List<CodeClass>>();
            var discoveredOperations = new List<RemoteOperation>();
            var cache = new CacheContext()
            {
                ClassesCache = classesCache,
                TypescriptTypesCache = new Dictionary<string, TypescriptType>()
            };

            foreach (var project in projects)
            {
                project.ForEachCodeClass(codeClass =>
                {
                    if (codeClass.Name.Equals(serviceName, StringComparison.OrdinalIgnoreCase) && IsRemoteServiceClass(codeClass))
                        rsClasses.Add(codeClass);
                    else
                    {
                        if (!classesCache.ContainsKey(codeClass.FullName))
                            classesCache.Add(codeClass.FullName, new List<CodeClass>());
                        classesCache[codeClass.FullName].Add(codeClass);
                    }

                });
            }

            foreach (var rs in rsClasses)
            {
                foreach (var codeMethod in rs.Members.OfType<CodeFunction>())
                {
                    if (!codeMethod.IsShared && codeMethod.Access == vsCMAccess.vsCMAccessPublic && codeMethod.FunctionKind == vsCMFunction.vsCMFunctionFunction)
                    {
                        if (IsIgnored(codeMethod))
                            continue;

                        var serviceOperation = new RemoteOperation();

                        if (codeMethod.Type.TypeKind == vsCMTypeRef.vsCMTypeRefVoid)
                        {
                            serviceOperation.ReturnType = new VoidType();
                        } 
                        else if (!codeMethod.Type.AsFullName.Equals(Consts.TaskVoidFullName, StringComparison.OrdinalIgnoreCase))
                        {
                            var returnedTypeName = codeMethod.Type.GetUnwrappedTaskTypeName();

                            serviceOperation.ReturnType = GetTypeScriptType(returnedTypeName, cache);
                            

                            //webMethod.ReturnedTypeScriptType = GetTypeScriptType(returnedTypeName, cache);
                            //if (_typeContext.IsGenericEnumerable(returnedTypeName))
                            //{
                            //    returnedTypeName = returnedTypeName.GetUnwrappedGenericTypeName();
                            //    webMethod.AssociatedTypeNames = new string[] { returnedTypeName };
                            //}
                            //else if (returnedTypeName.Contains('<'))
                            //{
                            //    // temp hack
                            //    webMethod.AssociatedTypeNames = returnedTypeName.TrimEnd('>').Split('<');
                            //    webMethod.AssociatedTypeNames[0] += "<T>";
                            //}
                            //else
                            //{
                            //    webMethod.AssociatedTypeNames = new string[] { returnedTypeName };
                            //}
                        }

                        serviceOperation.MethodVerb = GetMethodVerb(codeMethod);
                        serviceOperation.Name = GetOperationName(rs, codeMethod);

                        serviceOperation.Url = GetOperationUrl(rs, codeMethod);

                        
                        if (codeMethod.Parameters != null)
                        {
                            List<CodeClass> paramClass = null;

                            foreach (CodeParameter2 cp in codeMethod.Parameters)
                            {
                                if (ShouldSplitParamaters(cp) && cache.ClassesCache.TryGetValue(cp.Type.AsFullName, out paramClass))
                                {
                                    foreach (var pc in paramClass)
                                    {
                                        new ClassTraverser(pc, (property) =>
                                        {
                                            InterfacePropertyMember member;
                                            if (TryGetProperty(property, cache, out member))
                                                serviceOperation.Parameters.Add(new RemoteOperationParameter()
                                                {
                                                    Name = member.Name,
                                                    Type = GetTypeScriptType(property.Type, cache)
                                                });
                                        });
                                    }

                                    break; // only once?
                                }
                                else
                                    serviceOperation.Parameters.Add(new RemoteOperationParameter()
                                    {
                                        Name = cp.Name,
                                        //Optional = cp.ParameterKind == vsCMParameterKind.vsCMParameterKindOptional,
                                        Type = GetTypeScriptType(cp.Type, cache)
                                    });
                            }
                        }

                        discoveredOperations.Add(serviceOperation);
                    }
                }
            }

            return new RemoteServiceDiscovererResult()
            {
                Operations = discoveredOperations,
                InterfaceTypes = cache.TypescriptTypesCache.Where(kv => kv.Value is InterfaceType).Select(kv => kv.Value).Cast<InterfaceType>().ToList()
            };
        }

        private TypescriptType GetTypeScriptType(string typeName, CacheContext cache)
        {
            TypescriptType cachedType = null;

            if (cache.TypescriptTypesCache.TryGetValue(typeName, out cachedType))
                return cachedType;

            if (Utils.IsGenericEnumerable(typeName))
            {
                var elementTypeName = Utils.UnwrapGenericType(typeName);
                return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName,
                    new ArrayType(GetTypeScriptType(elementTypeName, cache)));

            }
            else if(Utils.IsNullable(typeName))
            {
                var elementTypeName = Utils.UnwrapGenericType(typeName);
                return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName,
                    new NullableType(GetTypeScriptType(elementTypeName, cache)));
            }
            //else if (typeName.Contains("<"))
            //{

            //}
            else if (cache.ClassesCache.ContainsKey(typeName))
            {
                var buildedInterface = BuildInterfaceType(typeName, cache);
                // let's check classes that inherits from our stuff
                if (cache.ClassesCache.TryGetValueOrDefault(typeName).With(x => x.FirstOrDefault()).Return(x => x.IsAbstract, false))
                {
                    cache.ClassesCache
                        .Where(kv => kv.Value.Any(cc => cc.Bases.OfType<CodeClass>().FirstOrDefault().Return(x => x.FullName.Equals(typeName, StringComparison.OrdinalIgnoreCase), false)))
                        .Select(kv => kv.Key)
                        .ToList()
                        .ForEach(t => GetTypeScriptType(t, cache));
                }
                return buildedInterface;
            }
            else
            {
                switch (typeName)
                {
                    case "System.Double":
                    case "System.Int16":
                    case "System.Int32":
                    case "System.Int64":
                    case "System.UInt16":
                    case "System.UInt32":
                    case "System.UInt64":
                    case "System.Decimal":
                    case "System.Byte":
                    case "System.SByte":
                    case "System.Single":
                        return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName, new NumberType());

                    case "System.String":
                    case "System.Guid":
                        return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName, new StringType());

                    case "System.DateTime":
                        return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName, new DateType());

                    default:
                        return cache.TypescriptTypesCache.TryAddAndReturnValue(typeName, new TypescriptType());
                }
            }
        }

        private TypescriptType GetTypeScriptType(CodeTypeRef codeType, CacheContext cache)
        {
            switch (codeType.TypeKind)
            {
                case vsCMTypeRef.vsCMTypeRefChar:
                case vsCMTypeRef.vsCMTypeRefString:
                    return new StringType();

                case vsCMTypeRef.vsCMTypeRefBool:
                    return new BooleanType();

                case vsCMTypeRef.vsCMTypeRefByte:
                case vsCMTypeRef.vsCMTypeRefDouble:
                case vsCMTypeRef.vsCMTypeRefInt:
                case vsCMTypeRef.vsCMTypeRefShort:
                case vsCMTypeRef.vsCMTypeRefFloat:
                case vsCMTypeRef.vsCMTypeRefLong:
                case vsCMTypeRef.vsCMTypeRefDecimal:
                    return new NumberType();

                default:
                    return TryResolveType(codeType, cache);
            }
        }

        private TypescriptType TryResolveType(CodeTypeRef codeType, CacheContext cache)
        {
            if (codeType.TypeKind == vsCMTypeRef.vsCMTypeRefArray)
            {
                return new ArrayType()
                {
                    ElementType = GetTypeScriptType(codeType.ElementType, cache)
                };
            }

            return GetTypeScriptType(codeType.AsFullName, cache);
        }

        private TypescriptType BuildInterfaceType(string typeName, CacheContext cache)
        {
            List<CodeClass> codeClasses;
            if (!cache.ClassesCache.TryGetValue(typeName, out codeClasses))
                return new TypescriptType();

            var firstCodeClass = codeClasses.First();

            var tsInterface = new InterfaceType()
                {
                    Name = GetInterfaceName(firstCodeClass),
                    IsGeneric = ((CodeClass2)firstCodeClass).IsGeneric
                };

            cache.TypescriptTypesCache.Add(typeName, tsInterface);

            foreach (var codeClass in codeClasses)
            {
                var baseClass = codeClass.Bases.OfType<CodeClass>().FirstOrDefault();

                if (baseClass != null && !baseClass.FullName.Equals(Consts.ObjectTypeName, StringComparison.OrdinalIgnoreCase))
                    tsInterface.BaseInterface = GetTypeScriptType(baseClass.FullName, cache) as InterfaceType;

                new ClassTraverser(codeClass, (property) =>
                {
                    InterfacePropertyMember member;
                    if (TryGetProperty(property, cache, out member))
                        tsInterface.Members.Add(member);
                });
            }

            return tsInterface;
        }

        private bool TryGetProperty(CodeProperty property, CacheContext cache, out InterfacePropertyMember member)
        {
            member = null;
            if (property.Access != vsCMAccess.vsCMAccessPublic)
                return false;

            var getter = property.Getter;
            if (getter == null)
                return false;

            member = new InterfacePropertyMember()
            {
                Name = GetPropertyName(property),
                Type = GetTypeScriptType(getter.Type, cache)
            };

            //if (values.CamelCase && values.Name == null)
            //    member.Name = member.Name.Substring(0, 1).ToLowerInvariant() + member.Name.Substring(1);

            return true;
        }

        class CacheContext
        {
            public IDictionary<string, List<CodeClass>> ClassesCache { get; set; }

            public IDictionary<string, TypescriptType> TypescriptTypesCache { get; set; }
        }

        protected abstract bool IsRemoteServiceClass(CodeClass codeClass);
        protected abstract bool IsIgnored(CodeFunction codeFunction);
        protected abstract string GetMethodVerb(CodeFunction codeFunction);

        protected virtual string GetInterfaceName(CodeClass codeClass)
        {
            return codeClass.Name;
        }

        protected virtual string GetPropertyName(CodeProperty codeProperty)
        {
            return codeProperty.Name;
        }

        protected virtual string GetOperationName(CodeClass serviceCodeClass, CodeFunction method)
        {
            return method.Name;
        }

        protected virtual string GetOperationUrl(CodeClass serviceCodeClass, CodeFunction method)
        {
            return string.Empty;
        }

        protected virtual bool ShouldSplitParamaters(CodeParameter2 cp)
        {
            return false;
        }
    }
}
