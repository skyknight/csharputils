﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;

namespace SkyKnight.T4TS.Core
{
    public class SignalRDiscoverer : BaseRemoteServiceDiscoverer
    {
        private const string HubTypeName = "Microsoft.AspNet.SignalR.Hub";

        protected override string GetMethodVerb(CodeFunction codeFunction)
        {
            return string.Empty;
        }

        protected override bool IsIgnored(CodeFunction codeFunction)
        {
            return false;
        }

        protected override bool IsRemoteServiceClass(CodeClass codeClass)
        {
            if (codeClass.Bases != null)
            {
                foreach (var bc in codeClass.Bases.OfType<CodeClass>())
                {
                    if (bc.FullName.Equals(Consts.ObjectTypeName, StringComparison.OrdinalIgnoreCase))
                        return false;
                    if (bc.FullName.Equals(HubTypeName, StringComparison.OrdinalIgnoreCase))
                        return true;
                    if (IsRemoteServiceClass(bc))
                        return true;
                }
            }
            return false;
        }
    }
}
