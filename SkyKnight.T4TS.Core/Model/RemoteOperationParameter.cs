﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class RemoteOperationParameter
    {
        public string Name { get; set; }

        public TypescriptType Type { get; set; }

        public override string ToString()
        {
            return string.Format("{0}:{1}", Name, Type);
        }
    }
}
