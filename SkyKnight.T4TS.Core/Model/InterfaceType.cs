﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class InterfaceType : TypescriptType
    {
        public string Name { get; set; }

        public List<InterfaceMember> Members { get; set; }

        public InterfaceType BaseInterface { get; set; }

        public bool IsGeneric { get; set; }

        public InterfaceType()
        {
            Members = new List<InterfaceMember>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
