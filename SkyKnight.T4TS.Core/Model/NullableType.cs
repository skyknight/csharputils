﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class NullableType : TypescriptType
    {
        public TypescriptType WrappedType { get; set; }

        public NullableType()
            : this(new TypescriptType())
        { }

        public NullableType(TypescriptType wrappedType)
        {
            WrappedType = wrappedType;
        }

        public override string ToString()
        {
            return string.Format("{0}", WrappedType ?? (object)"any");
        }
    }
}
