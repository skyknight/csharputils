﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class NumberType : TypescriptType
    {
        public override string ToString()
        {
            return "number";
        }
    }
}
