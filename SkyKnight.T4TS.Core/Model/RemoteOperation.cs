﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class RemoteOperation
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string MethodVerb { get; set; }

        public TypescriptType ReturnType { get; set; }

        public List<RemoteOperationParameter> Parameters { get; set; }

        public RemoteOperation()
        {
            Parameters = new List<RemoteOperationParameter>();
        }
    }
}
