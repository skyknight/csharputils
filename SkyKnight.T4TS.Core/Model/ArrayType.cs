﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class ArrayType : TypescriptType
    {
        public TypescriptType ElementType { get; set; }

        public ArrayType()
            : this(new TypescriptType())
        { }

        public ArrayType(TypescriptType elementType)
        {
            ElementType = elementType;
        }

        public override string ToString()
        {
            return string.Format("{0}[]", ElementType ?? (object)"any");
        }
    }
}
