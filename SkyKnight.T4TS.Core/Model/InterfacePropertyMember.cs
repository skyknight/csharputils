﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core.Model
{
    public class InterfacePropertyMember : InterfaceMember
    {
        public TypescriptType Type { get; set; }
    }
}
