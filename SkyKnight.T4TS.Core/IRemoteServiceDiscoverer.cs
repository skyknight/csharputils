﻿using EnvDTE;
using SkyKnight.T4TS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    public interface IRemoteServiceDiscoverer
    {
        RemoteServiceDiscovererResult DiscoverOperations(string serviceName, IEnumerable<Project> projects);
    }
}
