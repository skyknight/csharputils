﻿using EnvDTE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    public class WebApiDiscoverer : BaseRemoteServiceDiscoverer
    {
        private const string ApiControllerTypeName = "System.Web.Http.ApiController";

        private static readonly string RouteAttributeFullName = "System.Web.Http.RouteAttribute";
        private static readonly string RoutePrefixAttributeFullName = "System.Web.Http.RoutePrefixAttribute";
        private static readonly string NonActoinAttributeFullName = "System.Web.Http.NonActionAttribute";
        private static readonly string ActionNameAttributeFullName = "System.Web.Http.ActionNameAttribute";
        private static readonly string FromUriAttributeFullName = "System.Web.Http.FromUriAttribute";
        private static readonly string FromBodyAttributeFullName = "System.Web.Http.FromBodyAttribute";

        private static readonly string HttpMethodAttributeStart = "System.Web.Http.Http";
        private static readonly string[] HttpMethods = new string[] { "Get", "Post", "Options", "Head", "Delete", "Put" };

        protected override bool IsRemoteServiceClass(EnvDTE.CodeClass codeClass)
        {
            if (codeClass.Bases != null)
            {
                foreach (var bc in codeClass.Bases.OfType<CodeClass>())
                {
                    if (bc.FullName.Equals(Consts.ObjectTypeName, StringComparison.OrdinalIgnoreCase))
                        return false;
                    if (bc.FullName.Equals(ApiControllerTypeName, StringComparison.OrdinalIgnoreCase))
                        return true;
                    if (IsRemoteServiceClass(bc))
                        return true;
                }
            }
            return false;
        }

        protected override bool IsIgnored(EnvDTE.CodeFunction codeFunction)
        {
            CodeAttribute nonActionAttr = null;
            return codeFunction.Attributes.TryGetAttribute(NonActoinAttributeFullName, out nonActionAttr);
        }

        protected override string GetMethodVerb(EnvDTE.CodeFunction codeFunction)
        {
            var attributes = codeFunction.Attributes;
            foreach (var httpMethod in HttpMethods)
            {
                if(httpMethod.Equals(codeFunction.Name, StringComparison.OrdinalIgnoreCase))
                    return httpMethod.ToUpper();
                CodeAttribute attr = null;
                if (attributes.TryGetAttribute(HttpMethodAttributeStart + httpMethod + "Attribute", out attr))
                    return httpMethod.ToUpper();
            }

            return "GET";
        }

        protected override string GetInterfaceName(CodeClass codeClass)
        {
            return base.GetInterfaceName(codeClass);
        }

        protected override string GetOperationName(CodeClass serviceCodeClass, CodeFunction method)
        {
            string actionName = method.Name;
            CodeAttribute actionNameAttr = null;

            if (method.Attributes.TryGetAttribute(ActionNameAttributeFullName, out actionNameAttr))
            {
                var actionNameAttrValues = actionNameAttr.GetAttributeValues();
                if (!actionNameAttrValues.TryGetValue("Name", out actionName))
                    actionNameAttrValues.TryGetValue("", out actionName);
            }
            else if (method.Name.Equals("get", StringComparison.OrdinalIgnoreCase) 
                && Utils.IsGenericEnumerable(method.Type.AsFullName))
                actionName = "GetAll";
            return actionName;
        }

        protected override string GetOperationUrl(CodeClass serviceCodeClass, CodeFunction method)
        {
            CodeAttribute routePrefixAttr = null;
            string routePrefix = string.Empty;
            if (serviceCodeClass.Attributes.TryGetAttribute(RoutePrefixAttributeFullName, out routePrefixAttr))
            {
                var routePrefixAttrValues = routePrefixAttr.GetAttributeValues();

                if (routePrefixAttrValues.ContainsKey("Prefix"))
                    routePrefix = routePrefixAttrValues["Prefix"];
                else if (routePrefixAttrValues.ContainsKey(""))
                    routePrefix = routePrefixAttrValues[""];
            }
            var controllerShortName = serviceCodeClass.Name.Replace("Controller", string.Empty);
            string actionName = method.Name;
            var routeUrl = (string.IsNullOrWhiteSpace(routePrefix) ? controllerShortName : routePrefix) + "/" + actionName;

            CodeAttribute routeAttribute = null;

            if (method.Attributes.TryGetAttribute(RouteAttributeFullName, out routeAttribute))
            {
                var routeAttrValues = routeAttribute.GetAttributeValues();

                if (routeAttrValues.ContainsKey("Template"))
                    routeUrl = (string.IsNullOrWhiteSpace(routePrefix) ? "" : routePrefix + "/") + routeAttrValues["Template"];
                else if (routeAttrValues.ContainsKey(""))
                    routeUrl = (string.IsNullOrWhiteSpace(routePrefix) ? "" : routePrefix + "/") + routeAttrValues[""];
            }
            else if(HttpMethods.Any(h => h.Equals(method.Name, StringComparison.OrdinalIgnoreCase)))
            {
                routeUrl = (string.IsNullOrWhiteSpace(routePrefix) ? controllerShortName : routePrefix);
                if((actionName.Equals("get", StringComparison.OrdinalIgnoreCase) && method.Parameters.Count > 0) 
                    || actionName.Equals("put", StringComparison.OrdinalIgnoreCase) 
                    || actionName.Equals("delete", StringComparison.OrdinalIgnoreCase))
                {
                    routeUrl += "/{id}";
                }
            }

            return routeUrl;
        }

        protected override string GetPropertyName(CodeProperty codeProperty)
        {
            return base.GetPropertyName(codeProperty);
        }

        protected override bool ShouldSplitParamaters(EnvDTE80.CodeParameter2 cp)
        {
            CodeAttribute fromUriAttr = null;
            return (cp.Attributes.TryGetAttribute(FromUriAttributeFullName, out fromUriAttr) || cp.Attributes.TryGetAttribute(FromBodyAttributeFullName, out fromUriAttr));
        }
    }
}
