﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    class Utils
    {
        private static readonly string[] genericCollectionTypeStarts = new string[] {
            "System.Collections.Generic.List<",
            "System.Collections.Generic.IList<",
            "System.Collections.Generic.ICollection<",
            "System.Collections.Generic.IEnumerable<",
            "System.Linq.IQueryable<",
        };

        private static readonly string nullableTypeStart = "System.Nullable<";

        public static string UnwrapGenericType(string typeFullName)
        {
            int firstIndex = typeFullName.IndexOf('<');
            return firstIndex >= 0 ? typeFullName.Substring(firstIndex + 1, typeFullName.Length - firstIndex - 2) : typeFullName;
        }

        public static bool IsGenericEnumerable(string typeFullName)
        {
            return genericCollectionTypeStarts.Any(t => typeFullName.StartsWith(t));
        }

        public static bool IsNullable(string typeFullName)
        {
            return typeFullName.StartsWith(nullableTypeStart);
        }
    }
}
