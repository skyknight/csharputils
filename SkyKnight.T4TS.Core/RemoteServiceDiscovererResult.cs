﻿using SkyKnight.T4TS.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    public class RemoteServiceDiscovererResult
    {
        public IEnumerable<RemoteOperation> Operations { get; internal set; }

        public IEnumerable<InterfaceType> InterfaceTypes { get; internal set; }
    }
}
