﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;

namespace SkyKnight.T4TS.Core
{
    public class AspNetDiscoverer : BaseRemoteServiceDiscoverer
    {
        private const string ControllerTypeName = "Microsoft.AspNet.Mvc.Controller";

        private static readonly string RouteAttributeFullName = "Microsoft.AspNet.Mvc.RouteAttribute";
        private static readonly string NonActoinAttributeFullName = "Microsoft.AspNet.Mvc.NonActionAttribute";
        private static readonly string ActionNameAttributeFullName = "Microsoft.AspNet.Mvc.ActionNameAttribute";
        private static readonly string FromUriAttributeFullName = "Microsoft.AspNet.Mvc.FromUriAttribute";
        private static readonly string FromBodyAttributeFullName = "Microsoft.AspNet.Mvc.FromBodyAttribute";

        private static readonly string HttpMethodAttributeStart = "Microsoft.AspNet.Mvc.Http";
        private static readonly string[] HttpMethods = new string[] { "Get", "Post", "Options", "Head", "Delete", "Put" };

        protected override string GetMethodVerb(CodeFunction codeFunction)
        {
            var attributes = codeFunction.Attributes;
            foreach (var httpMethod in HttpMethods)
            {
                if (httpMethod.Equals(codeFunction.Name, StringComparison.OrdinalIgnoreCase))
                    return httpMethod.ToUpper();
                CodeAttribute attr = null;
                if (attributes.TryGetAttribute(HttpMethodAttributeStart + httpMethod + "Attribute", out attr))
                    return httpMethod.ToUpper();
            }

            return "GET";
        }

        protected override bool IsIgnored(CodeFunction codeFunction)
        {
            CodeAttribute nonActionAttr = null;
            return codeFunction.Attributes.TryGetAttribute(NonActoinAttributeFullName, out nonActionAttr);
        }

        protected override bool IsRemoteServiceClass(CodeClass codeClass)
        {
            // @TODO: support NonController
            if (codeClass.Bases != null)
            {
                foreach (var bc in codeClass.Bases.OfType<CodeClass>())
                {
                    if (bc.FullName.Equals(Consts.ObjectTypeName, StringComparison.OrdinalIgnoreCase))
                        return false;
                    if (bc.FullName.Equals(ControllerTypeName, StringComparison.OrdinalIgnoreCase))
                        return true;
                    if (IsRemoteServiceClass(bc))
                        return true;
                }
            }
            return false;
        }

        protected override string GetOperationName(CodeClass serviceCodeClass, CodeFunction method)
        {
            string actionName = method.Name;
            CodeAttribute actionNameAttr = null;

            if (method.Attributes.TryGetAttribute(ActionNameAttributeFullName, out actionNameAttr))
            {
                var actionNameAttrValues = actionNameAttr.GetAttributeValues();
                if (!actionNameAttrValues.TryGetValue("Name", out actionName))
                    actionNameAttrValues.TryGetValue("", out actionName);
            }
            else if (method.Name.Equals("get", StringComparison.OrdinalIgnoreCase)
                && Utils.IsGenericEnumerable(method.Type.AsFullName))
                actionName = "GetAll";
            return actionName;
        }

        protected override string GetOperationUrl(CodeClass serviceCodeClass, CodeFunction method)
        {
            CodeAttribute routePrefixAttr = null;
            string routePattern = string.Empty;
            if (serviceCodeClass.Attributes.TryGetAttribute(RouteAttributeFullName, out routePrefixAttr))
            {
                var routePatternAttrValues = routePrefixAttr.GetAttributeValues();

                if (routePatternAttrValues.ContainsKey("Template"))
                    routePattern = routePatternAttrValues["Template"];
                else if (routePatternAttrValues.ContainsKey(""))
                    routePattern = routePatternAttrValues[""];
            }
            var controllerShortName = serviceCodeClass.Name.Replace("Controller", string.Empty);
            string actionName = method.Name;
            CodeAttribute actionNameAttr = null;
            if (method.Attributes.TryGetAttribute(ActionNameAttributeFullName, out actionNameAttr))
            {
                var actionNameAttrValues = actionNameAttr.GetAttributeValues();
                if (!actionNameAttrValues.TryGetValue("Name", out actionName))
                    actionNameAttrValues.TryGetValue("", out actionName);
            }

            CodeAttribute routeActionAttribute = null;
            string routeActionPattern = string.Empty;
            var applyActionPart = true;

            if (method.Attributes.TryGetAttribute(RouteAttributeFullName, out routeActionAttribute))
            {
                var routeAttrValues = routeActionAttribute.GetAttributeValues();

                if (routeAttrValues.ContainsKey("Template"))
                    routeActionPattern = routeAttrValues["Template"];
                else if (routeAttrValues.ContainsKey(""))
                    routeActionPattern = routeAttrValues[""];
            }
            else
            {
                foreach (var httpMethod in HttpMethods)
                {
                    CodeAttribute attr = null;
                    if (method.Attributes.TryGetAttribute(HttpMethodAttributeStart + httpMethod + "Attribute", out attr))
                    {
                        var attrVals = attr.GetAttributeValues();
                        var tmpRouteUrl = string.Empty;

                        if (attrVals.ContainsKey("Template"))
                            tmpRouteUrl = attrVals["Template"];
                        else if (attrVals.ContainsKey(""))
                            tmpRouteUrl = attrVals[""];

                        if (!string.IsNullOrWhiteSpace(tmpRouteUrl))
                            routeActionPattern = tmpRouteUrl;
                        else
                            applyActionPart = false;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(routeActionPattern))
                routeActionPattern = actionName;

            if (string.IsNullOrWhiteSpace(routePattern))
                routePattern = controllerShortName;

            if (routePattern.Contains("[controller]") && routePattern.Contains("[action]"))
                return routePattern.Replace("[controller]", controllerShortName).Replace("[action]", actionName);

            if (routePattern.Contains("[controller]"))
                return routePattern.Replace("[controller]", controllerShortName) + (applyActionPart ? "/" + routeActionPattern : string.Empty);

            return routePattern + "/" + routeActionPattern;
        }

        protected override bool ShouldSplitParamaters(EnvDTE80.CodeParameter2 cp)
        {
            CodeAttribute fromUriAttr = null;
            return (cp.Attributes.TryGetAttribute(FromUriAttributeFullName, out fromUriAttr) || cp.Attributes.TryGetAttribute(FromBodyAttributeFullName, out fromUriAttr));
        }
    }
}
