﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.T4TS.Core
{
    class Consts
    {
        public const string TaskVoidFullName = "System.Threading.Tasks.Task";

        public const string ObjectTypeName = "System.Object";
    }
}
