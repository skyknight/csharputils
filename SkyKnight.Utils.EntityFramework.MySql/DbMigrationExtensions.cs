﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MySql
{
    public static class DbMigrationExtensions
    {
        #region sequences
        // http://www.microshell.com/database/mysql/emulating-nextval-function-to-get-sequence-in-mysql/
        // http://dev.mysql.com/doc/refman/5.0/en/innodb-locking-reads.html
        public static void CreateSequenceSupport(this DbMigration dbMigration)
        {
            dbMigration.CallSql(@"CREATE TABLE `sequence_data` (
    `sequence_name` varchar(100) NOT NULL,
    `sequence_increment` int(11) unsigned NOT NULL DEFAULT 1,
    `sequence_min_value` int(11) unsigned NOT NULL DEFAULT 1,
    `sequence_max_value` bigint(20) unsigned NOT NULL DEFAULT 18446744073709551615,
    `sequence_cur_value` bigint(20) unsigned DEFAULT 1,
    `sequence_cycle` boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`sequence_name`)
) ENGINE=MyISAM;");

            dbMigration.CallSql(@"CREATE FUNCTION `nextval` (`seq_name` varchar(100))
RETURNS bigint(20) NOT DETERMINISTIC
BEGIN
    DECLARE cur_val bigint(20);
 
    SELECT
        sequence_cur_value INTO cur_val
    FROM
        sequence_data
    WHERE
        sequence_name = seq_name
    ;
 
    IF cur_val IS NOT NULL THEN
        UPDATE
            sequence_data
        SET
            sequence_cur_value = IF (
                (sequence_cur_value + sequence_increment) > sequence_max_value,
                IF (
                    sequence_cycle = TRUE,
                    sequence_min_value,
                    NULL
                ),
                sequence_cur_value + sequence_increment
            )
        WHERE
            sequence_name = seq_name
        ;
    END IF;
 
    RETURN cur_val;
END;");
        }

        public static void CreateSequence(this DbMigration dbMigration,
    string name, int? startsWith = null, int? incrementBy = null, int? minValue = null, long? maxValue = null, bool? cycle = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");

            var sql = string.Format(@"INSERT INTO sequence_data (sequence_name, sequence_increment, sequence_min_value, sequence_max_value, sequence_cur_value, sequence_cycle)
VALUES ('{0}', {1}, {2}, {3}, {4}, {5})", name, incrementBy.HasValue ? incrementBy.Value : 1, minValue.HasValue ? minValue.Value : 1, maxValue.HasValue ? maxValue.Value : long.MaxValue, startsWith.HasValue ? startsWith.Value : 1, cycle == true ? "TRUE" : "FALSE");

            dbMigration.CallSql(sql);

            dbMigration.CallSql(string.Format(@"CREATE PROCEDURE {1} ()
BEGIN
  SELECT nextval('{0}') as NextValue;
END;", name, dbMigration.GetSequenceNextValueStoredProcedureName(name)));
        }

        public static void DropSequence(this DbMigration dbMigration, string name)
        {
            dbMigration.CallSql(string.Format("DROP PROCEDURE {0}", dbMigration.GetSequenceNextValueStoredProcedureName(name)));
            dbMigration.CallSql(string.Format("DELETE FROM sequence_data WHERE sequence_name = {0}", name));
        }
        #endregion

        public static void SqlScript(this DbMigration dbMigration, string scriptContent)
        {
            var scriptParser = new MySqlScriptParser();
            var statements = scriptParser.Parse(scriptContent);
            foreach (var stmnt in statements)
                dbMigration.CallSql(stmnt.text);
        }

        /// <summary>
        /// remember to add Allow User Variables=True to your connection string!
        /// </summary>
        /// <param name="dbMigration"></param>
        /// <param name="scriptPath"></param>
        public static void SqlScriptFile(this DbMigration dbMigration, string scriptPath)
        {
            if (string.IsNullOrWhiteSpace(scriptPath))
                throw new ArgumentException(nameof(scriptPath));

            scriptPath = Path.IsPathRooted(scriptPath) ? scriptPath : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, scriptPath);

            if (!File.Exists(scriptPath))
                throw new Exception("Script cannot be found.");

            var scriptContent = File.ReadAllText(scriptPath);
            dbMigration.SqlScript(scriptContent);
        }

        public static void SqlScriptResource(this DbMigration dbMigration, string resourceName, Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetExecutingAssembly();
            string scriptContent;
            using (var reader = new StreamReader(assembly.GetManifestResourceStream(resourceName)))
            {
                scriptContent = reader.ReadToEnd();
            }
            dbMigration.SqlScript(scriptContent);
        }
    }
}
