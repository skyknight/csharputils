﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MySql
{
    public class MySqlScriptParser
    {
        public List<ScriptStatement> Parse(string query)
        {
            return BreakIntoStatements(query, ";", false, false);
        }

        #region stuff copied from MySql Connector source code
        private List<int> BreakScriptIntoLines(string query)
        {
            List<int> lineNumbers = new List<int>();

            StringReader sr = new StringReader(query);
            string line = sr.ReadLine();
            int pos = 0;
            while (line != null)
            {
                lineNumbers.Add(pos);
                pos += line.Length;
                line = sr.ReadLine();
            }
            return lineNumbers;
        }

        private static int FindLineNumber(int position, List<int> lineNumbers)
        {
            int i = 0;
            while (i < lineNumbers.Count && position < lineNumbers[i])
                i++;
            return i;
        }

        private List<ScriptStatement> BreakIntoStatements(string query, string delimiter, bool ansiQuotes, bool noBackslashEscapes)
        {
            string currentDelimiter = delimiter;
            int startPos = 0;
            List<ScriptStatement> statements = new List<ScriptStatement>();
            List<int> lineNumbers = BreakScriptIntoLines(query);
            MySqlTokenizer tokenizer = new MySqlTokenizer(query);

            tokenizer.AnsiQuotes = ansiQuotes;
            tokenizer.BackslashEscapes = !noBackslashEscapes;

            string token = tokenizer.NextToken();
            while (token != null)
            {
                if (!tokenizer.Quoted)
                {
                    if (token.ToLower(CultureInfo.InvariantCulture) == "delimiter")
                    {
                        tokenizer.NextToken();
                        AdjustDelimiterEnd(query, tokenizer);
                        currentDelimiter = query.Substring(tokenizer.StartIndex,
                          tokenizer.StopIndex - tokenizer.StartIndex).Trim();
                        startPos = tokenizer.StopIndex;
                    }
                    else
                    {
                        // this handles the case where our tokenizer reads part of the
                        // delimiter
                        if (currentDelimiter.StartsWith(token, StringComparison.OrdinalIgnoreCase))
                        {
                            if ((tokenizer.StartIndex + currentDelimiter.Length) <= query.Length)
                            {
                                if (query.Substring(tokenizer.StartIndex, currentDelimiter.Length) == currentDelimiter)
                                {
                                    token = currentDelimiter;
                                    tokenizer.Position = tokenizer.StartIndex + currentDelimiter.Length;
                                    tokenizer.StopIndex = tokenizer.Position;
                                }
                            }
                        }

                        int delimiterPos = token.IndexOf(currentDelimiter, StringComparison.OrdinalIgnoreCase);
                        if (delimiterPos != -1)
                        {
                            int endPos = tokenizer.StopIndex - token.Length + delimiterPos;
                            if (tokenizer.StopIndex == query.Length - 1)
                                endPos++;
                            string currentQuery = query.Substring(startPos, endPos - startPos);
                            ScriptStatement statement = new ScriptStatement();
                            statement.text = currentQuery.Trim();
                            statement.line = FindLineNumber(startPos, lineNumbers);
                            statement.position = startPos - lineNumbers[statement.line];
                            statements.Add(statement);
                            startPos = endPos + currentDelimiter.Length;
                        }
                    }
                }
                token = tokenizer.NextToken();
            }

            // now clean up the last statement
            if (startPos < query.Length - 1)
            {
                string sqlLeftOver = query.Substring(startPos).Trim();
                if (!String.IsNullOrEmpty(sqlLeftOver))
                {
                    ScriptStatement statement = new ScriptStatement();
                    statement.text = sqlLeftOver;
                    statement.line = FindLineNumber(startPos, lineNumbers);
                    statement.position = startPos - lineNumbers[statement.line];
                    statements.Add(statement);
                }
            }
            return statements;
        }

        private void AdjustDelimiterEnd(string query, MySqlTokenizer tokenizer)
        {
            if (tokenizer.StopIndex < query.Length)
            {
                int pos = tokenizer.StopIndex;
                char c = query[pos];

                while (!Char.IsWhiteSpace(c) && pos < (query.Length - 1))
                {
                    c = query[++pos];
                }
                tokenizer.StopIndex = pos;
                tokenizer.Position = pos;
            }
        }

        internal class MySqlTokenizer
        {
            private string sql;

            private int startIndex;
            private int stopIndex;

            private bool ansiQuotes;
            private bool backslashEscapes;
            private bool returnComments;
            private bool multiLine;
            private bool sqlServerMode;

            private bool quoted;
            private bool isComment;

            private int pos;

            public MySqlTokenizer()
            {
                backslashEscapes = true;
                multiLine = true;
                pos = 0;
            }

            public MySqlTokenizer(string input)
              : this()
            {
                sql = input;
            }

            #region Properties

            public string Text
            {
                get { return sql; }
                set { sql = value; pos = 0; }
            }

            public bool AnsiQuotes
            {
                get { return ansiQuotes; }
                set { ansiQuotes = value; }
            }

            public bool BackslashEscapes
            {
                get { return backslashEscapes; }
                set { backslashEscapes = value; }
            }

            public bool MultiLine
            {
                get { return multiLine; }
                set { multiLine = value; }
            }

            public bool SqlServerMode
            {
                get { return sqlServerMode; }
                set { sqlServerMode = value; }
            }

            public bool Quoted
            {
                get { return quoted; }
                private set { quoted = value; }
            }

            public bool IsComment
            {
                get { return isComment; }
            }

            public int StartIndex
            {
                get { return startIndex; }
                set { startIndex = value; }
            }

            public int StopIndex
            {
                get { return stopIndex; }
                set { stopIndex = value; }
            }

            public int Position
            {
                get { return pos; }
                set { pos = value; }
            }

            public bool ReturnComments
            {
                get { return returnComments; }
                set { returnComments = value; }
            }

            #endregion

            public List<string> GetAllTokens()
            {
                List<string> tokens = new List<string>();
                string token = NextToken();
                while (token != null)
                {
                    tokens.Add(token);
                    token = NextToken();
                }
                return tokens;
            }

            public string NextToken()
            {
                while (FindToken())
                {
                    string token = sql.Substring(startIndex, stopIndex - startIndex);
                    return token;
                }
                return null;
            }

            public static bool IsParameter(string s)
            {
                if (String.IsNullOrEmpty(s)) return false;
                if (s[0] == '?') return true;
                if (s.Length > 1 && s[0] == '@' && s[1] != '@') return true;
                return false;
            }

            public string NextParameter()
            {
                while (FindToken())
                {
                    if ((stopIndex - startIndex) < 2) continue;
                    char c1 = sql[startIndex];
                    char c2 = sql[startIndex + 1];
                    if (c1 == '?' ||
                        (c1 == '@' && c2 != '@'))
                        return sql.Substring(startIndex, stopIndex - startIndex);
                }
                return null;
            }

            public bool FindToken()
            {
                isComment = quoted = false;  // reset our flags
                startIndex = stopIndex = -1;

                while (pos < sql.Length)
                {
                    char c = sql[pos++];
                    if (Char.IsWhiteSpace(c)) continue;

                    if (c == '`' || c == '\'' || c == '"' || (c == '[' && SqlServerMode))
                        ReadQuotedToken(c);
                    else if (c == '#' || c == '-' || c == '/')
                    {
                        if (!ReadComment(c))
                            ReadSpecialToken();
                    }
                    else
                        ReadUnquotedToken();
                    if (startIndex != -1) return true;
                }
                return false;
            }

            public string ReadParenthesis()
            {
                StringBuilder sb = new StringBuilder("(");
                int start = StartIndex;
                string token = NextToken();
                while (true)
                {
                    if (token == null)
                        throw new InvalidOperationException("Unable to parse SQL");
                    sb.Append(token);
                    if (token == ")" && !Quoted) break;
                    token = NextToken();
                }
                return sb.ToString();
            }

            private bool ReadComment(char c)
            {
                // make sure the comment starts correctly
                if (c == '/' && (pos >= sql.Length || sql[pos] != '*')) return false;
                if (c == '-' && ((pos + 1) >= sql.Length || sql[pos] != '-' || sql[pos + 1] != ' ')) return false;

                string endingPattern = "\n";
                if (sql[pos] == '*')
                    endingPattern = "*/";

                int startingIndex = pos - 1;

                int index = sql.IndexOf(endingPattern, pos);
                if (endingPattern == "\n")
                    index = sql.IndexOf('\n', pos);
                if (index == -1)
                    index = sql.Length - 1;
                else
                    index += endingPattern.Length;

                pos = index;
                if (ReturnComments)
                {
                    startIndex = startingIndex;
                    stopIndex = index;
                    isComment = true;
                }
                return true;
            }

            private void CalculatePosition(int start, int stop)
            {
                startIndex = start;
                stopIndex = stop;
                if (!MultiLine) return;
            }

            private void ReadUnquotedToken()
            {
                startIndex = pos - 1;

                if (!IsSpecialCharacter(sql[startIndex]))
                {
                    while (pos < sql.Length)
                    {
                        char c = sql[pos];
                        if (Char.IsWhiteSpace(c)) break;
                        if (IsSpecialCharacter(c)) break;
                        pos++;
                    }
                }

                Quoted = false;
                stopIndex = pos;
            }

            private void ReadSpecialToken()
            {
                startIndex = pos - 1;

                Debug.Assert(IsSpecialCharacter(sql[startIndex]));

                stopIndex = pos;
                Quoted = false;
            }

            /// <summary>
            ///  Read a single quoted identifier from the stream
            /// </summary>
            /// <param name="quoteChar"></param>
            /// <returns></returns>
            private void ReadQuotedToken(char quoteChar)
            {
                if (quoteChar == '[')
                    quoteChar = ']';
                startIndex = pos - 1;
                bool escaped = false;

                bool found = false;
                while (pos < sql.Length)
                {
                    char c = sql[pos];

                    if (c == quoteChar && !escaped)
                    {
                        found = true;
                        break;
                    }

                    if (escaped)
                        escaped = false;
                    else if (c == '\\' && BackslashEscapes)
                        escaped = true;
                    pos++;
                }
                if (found) pos++;
                Quoted = found;
                stopIndex = pos;
            }

            private bool IsQuoteChar(char c)
            {
                return c == '`' || c == '\'' || c == '\"';
            }

            private bool IsParameterMarker(char c)
            {
                return c == '@' || c == '?';
            }

            private bool IsSpecialCharacter(char c)
            {
                if (Char.IsLetterOrDigit(c) ||
                    c == '$' || c == '_' || c == '.') return false;
                if (IsParameterMarker(c)) return false;
                return true;
            }
        }
        #endregion
    }

    public struct ScriptStatement
    {
        public string text;
        public int line;
        public int position;
    }
}
