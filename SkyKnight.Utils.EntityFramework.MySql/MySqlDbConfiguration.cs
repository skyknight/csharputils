﻿using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MySql
{
    // http://dan.cx/2015/08/entity-framework-6-mysql-aspnet
    public class MySqlDbConfiguration : DbConfiguration
    {
        public MySqlDbConfiguration()
        {
            // Attempt to register ADO.NET provider 
            try
            {
                var dataSet = (DataSet)ConfigurationManager.GetSection("system.data");
                var mysqlRow = dataSet.Tables[0].Rows.Find("MySql.Data.MySqlClient");
                if (mysqlRow != null)
                    dataSet.Tables[0].Rows.Remove(mysqlRow);
                dataSet.Tables[0].Rows.Add(
                    "MySQL Data Provider",
                    ".Net Framework Data Provider for MySQL",
                    "MySql.Data.MySqlClient",
                    typeof(MySqlClientFactory).AssemblyQualifiedName
                );
            }
            catch (ConstraintException)
            {
                // MySQL provider is already installed, just ignore the exception 
            }

            SetProviderServices("MySql.Data.MySqlClient", new MySqlProviderServices());
            SetProviderFactoryResolver(new MySqlProviderFactoryResolver());
            SetManifestTokenResolver(new MySqlManifestTokenResolver());
        }
    }
}
