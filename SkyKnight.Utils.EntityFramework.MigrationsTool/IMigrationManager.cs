﻿using System;

namespace SkyKnight.Utils.EntityFramework.MigrationsTool
{
	public interface IMigrationManager
	{
		string MigrationFolder { get; set; }
		void To (string migration);
		void Up ();
		void Add (string title);
	}
}

