﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SkyKnight.Utils.EntityFramework.MigrationsTool
{
    public class MigrationTool : MarshalByRefObject
    {
        public void Run(
            string assemblyPath,
            string dbProvider,
            string connectionString,
            string command,
            string migrationsPath,
            string targetMigration)
        {
            try
            {
                var mgrMan = BuildMigrationManager(assemblyPath, dbProvider, connectionString);

                switch (command.ToUpper())
                {
                    case "ADD":
                        {
                            Console.WriteLine("Specify Migration Title");
                            var migrationName = Console.ReadLine();
                            mgrMan.Add(migrationName);
                            Console.WriteLine("Migration Added");
                        }
                        break;
                    case "UP":
                        {
                            mgrMan.Up();
                            Console.WriteLine("Database version is upped");

                        }
                        break;
                    case "TO":
                        {
                            mgrMan.To(targetMigration);
                            Console.WriteLine("Database migrated");
                        }
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        private static IMigrationManager BuildMigrationManager(string assemblyPath, string dbProvider, string connectionString)
        {
            var baseDbContextType = typeof(DbContext);
            var baseConfigurationType = typeof(DbMigrationsConfiguration);
            var assembly = Assembly.LoadFrom(assemblyPath);
            var dbContextType = assembly.GetTypes().FirstOrDefault(t => t.IsSubclassOf(baseDbContextType));
            if (dbContextType == null)
                throw new Exception("No database context in given assembly.");
            var configurationType = assembly.GetTypes().FirstOrDefault(t => t.IsSubclassOf(baseConfigurationType));
            if (configurationType == null)
                throw new Exception("No configuration class in given assembly.");
            var configuration = Activator.CreateInstance(configurationType) as DbMigrationsConfiguration;
            if(!string.IsNullOrWhiteSpace(connectionString))
                configuration.TargetDatabase = new System.Data.Entity.Infrastructure.DbConnectionInfo(connectionString, dbProvider);
            configuration.AutomaticMigrationsEnabled = true;
            return new MigrationManager(configuration);
        }
    }
}
