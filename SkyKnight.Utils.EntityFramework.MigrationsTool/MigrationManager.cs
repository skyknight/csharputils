﻿using System;
using System.Data.Entity.Migrations;
using System.IO;
using System.Data.Entity.Migrations.Design;
using System.Data.Entity;

namespace SkyKnight.Utils.EntityFramework.MigrationsTool
{
	public class MigrationManager : IMigrationManager
	{
		private DbMigrationsConfiguration _config;

		public string MigrationFolder
		{
			get;
			set;
		}

		public MigrationManager (DbMigrationsConfiguration configuration)
		{
			_config = configuration;
		}

		public void To (string migration)
		{
			var migrator = new DbMigrator (_config);
			migrator.Update (migration);
		}

		public void Up ()
		{
			var migrator = new DbMigrator (_config);
			migrator.Update ();
		}

		public void Add (string title)
		{
			if (string.IsNullOrWhiteSpace(MigrationFolder))
				throw new Exception("MigrationFolder is empty");

			var scaffolder = new MigrationScaffolder (_config);

			var migration = scaffolder.Scaffold (title);

			//Because monodevelop/xamarin doesn't support resxfilecodegenerator, we can't user resx files properly
			//Another issue is lack of power shell support which force us to generate all cs/resx files manually and add them to 
			//projet, what is why we are going to put all migration data to one file using ugly hacks
			//Code might stop working on future versions of Entity framework.

			//Add Migration Namespace
			migration.UserCode = "using System.Data.Entity.Migrations.Infrastructure;nn" + migration.UserCode;

			//Add IMigrationMetadata interface
			migration.UserCode = migration.UserCode.Replace ("DbMigration", "DbMigration, IMigrationMetadata");

			//Insert IMigrationMetadate implementation
			migration.UserCode = migration.UserCode.Replace ("public override void Up()",
				string.Format ("tstring IMigrationMetadata.Id {{nttt" +
					"get {{ return \"{0}\"; }}ntt" +
					"}}nntt" +
					"string IMigrationMetadata.Source {{nttt" +
					"get {{ return null; }}ntt" +
					"}}nnntt" +
					"string IMigrationMetadata.Target {{nttt" +
					"get {{ return \"{1}\"; }}ntt}}" +
					"nntpublic override void Up()", migration.MigrationId, migration.Resources ["Target"]));

			File.WriteAllText (Path.Combine (MigrationFolder, migration.MigrationId + ".cs"), migration.UserCode);
		}
	}
}

