﻿using System;
using System.Linq;
using System.Collections;
using System.Reflection;
using System.Data.Entity;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using NDesk.Options;

namespace SkyKnight.Utils.EntityFramework.MigrationsTool
{
	// http://demibyte.com/2015/04/06/entity-framework-with-mysql-on-mac-os/
	class MainClass
	{
		public static void Main(string[] args)
		{
            string dbProvider, assemblyPath, connectionString, command, migrationsPath, targetMigration, configurationPath;
            dbProvider = assemblyPath = connectionString = command = migrationsPath = targetMigration = configurationPath = string.Empty;

            var p = new OptionSet()
            {
                { "db|dbProvider=", "Database provider", v => dbProvider = v },
                { "ap|assemblyPath=", "Path to assembly with DbContext", v => assemblyPath = v },
                { "cs|connectionString=", "Connection string", v => connectionString = v },
                { "cmd|command=", "Command", v => command = v },
                { "mp|migrationsPath=", "Path where migrations code is stored", v => migrationsPath = v },
                { "t|targetMigration=", "Target migration", v => targetMigration = v },
                { "cfg|configurationPath=", "Path to configuration file", v => configurationPath = v }
            };

            var extraArgs = p.Parse(args);

			try
			{
                if (string.IsNullOrWhiteSpace(configurationPath))
                    configurationPath = assemblyPath + ".config";

                AppDomainSetup setup = new AppDomainSetup();
                setup.ApplicationName = "MigrationTool Sandbox";
                setup.ConfigurationFile = configurationPath;

                var domain = AppDomain.CreateDomain(
                    "MigrationTool Sandbox",
                    AppDomain.CurrentDomain.Evidence,
                    setup);

                var migrationTool = (MigrationTool)domain.CreateInstance(Assembly.GetExecutingAssembly().FullName, typeof(MigrationTool).FullName).Unwrap();
                migrationTool.Run(assemblyPath, dbProvider, connectionString, command, migrationsPath, targetMigration);
            }
			catch(Exception ex)
			{
				Console.WriteLine(ex);
			}
		}


	}
}
