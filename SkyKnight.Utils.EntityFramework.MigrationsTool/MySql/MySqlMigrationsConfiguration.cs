﻿using System;
using System.Data.Entity.Migrations;
using MySql.Data.Entity;
using System.Data.Entity;

namespace SkyKnight.Utils.EntityFramework.MigrationsTool.MySql
{
	public class MySqlMigrationsConfiguration<T> : DbMigrationsConfiguration<T> where T : DbContext
	{
		public MySqlMigrationsConfiguration ()
		{
			SetSqlGenerator ("MySql.Data.MySqlClient", new MySqlMigrationSqlGenerator ());
		}
	}
}

