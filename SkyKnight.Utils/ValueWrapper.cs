﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SkyKnight.Utils
{
    public class ValueWrapper : IXmlSerializable
    {
        public string DataTypeName { get; set; }

        public object Value { get; set; }

        private readonly XmlSerializer _dataTypeNameSerializer = new XmlSerializer(typeof(string));

        public ValueWrapper()
        {
        }

        public ValueWrapper(object value)
        {
            Value = value;
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                return;
            }
            // Move past container
            if (reader.NodeType == XmlNodeType.Element && !reader.Read())
            {
                throw new XmlException("Error in Deserialization of ValueWrapper");
            }

            reader.ReadStartElement("wrapper");
            reader.ReadStartElement("dataTypeName");
            DataTypeName = (string)_dataTypeNameSerializer.Deserialize(reader);
            reader.ReadEndElement();

            var dataType = Type.GetType(DataTypeName);
            var valueSerializer = new XmlSerializer(dataType);

            reader.ReadStartElement("value");
            Value = valueSerializer.Deserialize(reader);
            reader.ReadEndElement();
            reader.ReadEndElement();

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
            else
            {
                throw new XmlException("Error in Deserialization of ValueWrapper");
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var dataType = Value == null ? Type.GetType(DataTypeName ?? "System.Object") : Value.GetType();

            writer.WriteStartElement("wrapper");
            writer.WriteStartElement("dataTypeName");
            _dataTypeNameSerializer.Serialize(writer, DataTypeName ?? dataType.FullName);
            writer.WriteEndElement();

            var valueSerializer = new XmlSerializer(dataType);

            writer.WriteStartElement("value");
            valueSerializer.Serialize(writer, Value);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
