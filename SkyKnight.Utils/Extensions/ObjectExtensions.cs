﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyKnight.Utils
{
    public static class ObjectExtensions
    {
        public static T GetPropertyValue<T>(this object obj, string key)
        {
            var pi = obj.GetType().GetProperty(key);
            if (pi != null)
                return (T)pi.GetValue(obj, null);
            return default(T);
        }

        public static bool TryInvokeMethod(this object obj, string methodName, object[] parameters, out object result)
        {
            result = null;
            var method = obj.GetType().GetMethod(methodName);
            if(method != null)
            {
                result = method.Invoke(obj, parameters);
                return true;
            }
            return false;
        }
    }
}
