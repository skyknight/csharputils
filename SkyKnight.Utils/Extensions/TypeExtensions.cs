﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyKnight.Utils
{
    public static class TypeExtensions
    {
        public static Type GetTopBaseType(this Type type)
        {
            while (type != null && type.BaseType != typeof(Object))
            {
                type = type.BaseType;
            }
            return type;
        }
    }
}
