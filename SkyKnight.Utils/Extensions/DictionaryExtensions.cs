﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyKnight.Utils
{
    public static class DictionaryExtensions
    {
        public static TValue TryGetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key)
        {
            TValue v = default(TValue);
            if (key == null)
                return v;
            dic.TryGetValue(key, out v);
            return v;
        }

        public static bool TryGetValue<TValue>(this IDictionary<string, object> dic, string key, out TValue value, Func<object, TValue> converter = null)
        {
            value = default(TValue);
            object v = null;
            if (dic.TryGetValue(key, out v))
            {
                if (v is TValue)
                    value = (TValue)v;
                else if (converter != null)
                    value = converter(v);
                else
                    throw new Exception(string.Format("Cannot convert from {0} to {1}.", v.GetType(), typeof(TValue)));
                return true;
            }
            return false;
        }

        public static bool TryAdd<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            if (dic.ContainsKey(key))
                return false;
            dic.Add(key, value);
            return true;
        }

        public static TValue AddAndReturnValue<TKey, TValue>(this IDictionary<TKey, TValue> dic, TKey key, TValue value)
        {
            dic.Add(key, value);
            return value;
        }

        // Works in C#3/VS2008:
        // Returns a new dictionary of this ... others merged leftward.
        // Keeps the type of 'this', which must be default-instantiable.
        // Example: 
        //   result = map.MergeLeft(other1, other2, ...)
        public static T MergeLeft<T, K, V>(this T me, params IDictionary<K, V>[] others)
            where T : IDictionary<K, V>, new()
        {
            T newMap = new T();
            foreach (IDictionary<K, V> src in
                (new List<IDictionary<K, V>> { me }).Concat(others))
            {
                // ^-- echk. Not quite there type-system.
                foreach (KeyValuePair<K, V> p in src)
                {
                    newMap[p.Key] = p.Value;
                }
            }
            return newMap;
        }

        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dic, IEnumerable<KeyValuePair<TKey, TValue>> dic2)
        {
            foreach (var item in dic2)
            {
                dic.Add(item.Key, item.Value);
            }
        }
    }
}
