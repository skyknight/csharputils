﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyKnight.Utils
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int size)
        {
            var partition = new List<T>(size);
            var counter = 0;

            using (var enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    partition.Add(enumerator.Current);
                    counter++;
                    if (counter % size == 0)
                    {
                        yield return partition.ToList();
                        partition.Clear();
                        counter = 0;
                    }
                }

                if (counter != 0)
                    yield return partition;
            }
        }

        public static T[,] ToTwoDimensionalArray<T>(this IEnumerable<T[]> enumerable)
        {
            if (!enumerable.Any())
                return new T[0, 0];

            var array2d = new T[enumerable.Count(), enumerable.First().Length];

            var h = 0;
            foreach (var array in enumerable)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array2d[h, i] = array[i];
                }
                h++;
            }

            return array2d;
        }

        public static IList<T> RemoveNearDuplicates<T>(this IEnumerable<T> list)
        {
            var resultList = new List<T>();

            T previoutItem = default(T);
            var isFirst = true;

            foreach (var item in list)
            {
                if (isFirst)
                {
                    previoutItem = item;
                    resultList.Add(item);
                    isFirst = false;
                    continue;
                }

                if (!item.Equals(previoutItem))
                {
                    resultList.Add(item);
                    previoutItem = item;
                }
            }

            return resultList;
        }
    }
}
