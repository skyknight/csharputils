﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SkyKnight.Utils
{
    public static class SerializableDictionaryExtensions
    {
        public static byte[] SerializeToByteArray<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            var dic = new SerializableDictionary<TKey, TValue>();
            foreach (var v in dictionary)
                dic.Add(v.Key, v.Value);

            using (var ms = new MemoryStream())
            {
                var serializer = new XmlSerializer(typeof(SerializableDictionary<TKey, TValue>));
                serializer.Serialize(ms, dic);
                ms.Position = 0;
                return ms.ToArray();
            }
        }

        public static IDictionary<TKey, TValue> DeserializeAsDictionary<TKey, TValue>(this byte[] data)
        {
            using (var ms = new MemoryStream(data))
            {
                var serializer = new XmlSerializer(typeof(SerializableDictionary<TKey, TValue>));
                return (IDictionary<TKey, TValue>)serializer.Deserialize(ms);
            }
        }
    }
}
