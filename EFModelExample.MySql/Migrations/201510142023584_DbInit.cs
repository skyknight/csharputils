namespace EFModelExample.MySql.Migrations
{
    using SkyKnight.Utils.EntityFramework.MySql;
    using System;
    using System.Data.Entity.Migrations;

    public partial class DbInit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        OrderItemId = c.Guid(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        Order_OrderId = c.Guid(),
                    })
                .PrimaryKey(t => t.OrderItemId)
                .ForeignKey("dbo.Orders", t => t.Order_OrderId)
                .Index(t => t.Order_OrderId);
            
            CreateTable(
                "dbo.OrderPersons",
                c => new
                    {
                        OrderPersonId = c.Guid(nullable: false, identity: true),
                        PersonId = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.OrderPersonId)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Guid(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Date = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.OrderId);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PersonId = c.Guid(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        BirthDay = c.DateTime(precision: 0)
                    })
                .PrimaryKey(t => t.PersonId);

            this.SqlScriptResource("EFModelExample.MySql.Migrations.TestScript.sql", typeof(Configuration).Assembly);

            CreateStoredProcedure(
                "dbo.Person_Insert",
                p => new
                    {
                        Name = p.String(maxLength: 1073741823, unicode: false),
                        BirthDay = p.DateTime(),
                    },
                body:
                    @"SET SESSION sql_mode='ANSI';INSERT INTO `People`(
                      `Name`, 
                      `BirthDay`) VALUES (
                      @Name, 
                      @BirthDay);
                      SELECT
                      `PersonId`, 
                      `OrdersCount`
                      FROM `PeopleView`
                       WHERE  row_count() > 0 AND `PersonId`=ANY(SELECT guid FROM tmpIdentity_People);"
            );
            
            CreateStoredProcedure(
                "dbo.Person_Update",
                p => new
                    {
                        PersonId = p.Guid(),
                        Name = p.String(maxLength: 1073741823, unicode: false),
                        BirthDay = p.DateTime(),
                    },
                body:
                    @"UPDATE `People` SET `Name`=@Name, `BirthDay`=@BirthDay WHERE `PersonId` = @PersonId;
                      SELECT
                      `OrdersCount`
                      FROM `PeopleView`
                       WHERE  row_count() > 0 and `PersonId` = @PersonId;"
            );
            
            CreateStoredProcedure(
                "dbo.Person_Delete",
                p => new
                    {
                        PersonId = p.Guid(),
                    },
                body:
                    @"DELETE FROM `People` WHERE `PersonId` = @PersonId;"
            );
            
        }
        
        public override void Down()
        {
            DropStoredProcedure("dbo.Person_Delete");
            DropStoredProcedure("dbo.Person_Update");
            DropStoredProcedure("dbo.Person_Insert");
            DropForeignKey("dbo.OrderPersons", "PersonId", "dbo.PeopleView");
            DropForeignKey("dbo.OrderPersons", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderItems", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderPersons", new[] { "PersonId" });
            DropIndex("dbo.OrderPersons", new[] { "OrderId" });
            DropIndex("dbo.OrderItems", new[] { "Order_OrderId" });
            DropTable("dbo.PeopleView");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderPersons");
            DropTable("dbo.OrderItems");
        }
    }
}
