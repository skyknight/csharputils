﻿DELIMITER $$
CREATE FUNCTION CountOrdersForPerson
(
	personId CHAR(36)
)
RETURNS INT
BEGIN
	DECLARE ordersCount INT DEFAULT 0;

	SELECT COUNT(*) INTO @ordersCount FROM OrderPersons op WHERE op.PersonId = @personId;

	RETURN ordersCount;
END
$$
CREATE VIEW PeopleView AS SELECT p.PersonId, p.Name, p.BirthDay, CountOrdersForPerson(p.PersonId) AS OrdersCount FROM People p
$$
DELIMITER ;