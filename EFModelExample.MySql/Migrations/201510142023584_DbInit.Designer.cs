// <auto-generated />
namespace EFModelExample.MySql.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class DbInit : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DbInit));
        
        string IMigrationMetadata.Id
        {
            get { return "201510142023584_DbInit"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
